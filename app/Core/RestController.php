<?php

namespace App\Core;

use CodeIgniter\RESTful\ResourceController;

class RestController extends ResourceController
{

    public $session;
    public $validation;

    function __construct()
    {
        $this->session = \Config\Services::session();
        $this->validation = \Config\Services::validation();
    }
}
