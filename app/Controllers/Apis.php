<?php

namespace App\Controllers;

use App\Core\RestController;
use App\Entities\CaPolizaXml_entity;
use App\Entities\DePolizaXml_entity;
use App\Entities\DePersonasXmlConfig_entity;

use App\Models\CaPolizasXml_model;
use App\Models\DePolizasXml_model;
use App\Models\DePersonasXmlConfig_model;

class Apis extends RestController
{

    public $validation;

    protected $format_response = [
        'status' => 'ok',
        'data' => false,
        'message' => false
    ];

    protected $modelName = CaPolizasXml_model::class;
    protected $format    = 'json';

    public function __construct()
    {
        parent::__construct();
    }

    public function comprobante_guardar(){

        $rules = [
            "descripcion" => [
                "label" => "Descripción", 
                "rules" => "required|max_length[500]"
            ],
            "archivo_pdf" => [
                "label" => "Archivo PDF", 
                "rules" => "uploaded[archivo_pdf]|max_size[archivo_pdf,2048]|mime_in[archivo_pdf,application/pdf]"
            ],
            "archivo_xml" => [
                "label" => "Archivo XML", 
                "rules" => "uploaded[archivo_xml]|max_size[archivo_xml,2048]"
                // "rules" => "uploaded[archivo_xml]|max_size[archivo_xml,2048]|mime_in[archivo_xml,text/xml,application/xml]"
            ]
        ];

        if ($this->validate($rules)) {

            $DePolizasXmlModel = new DePolizasXml_model();

            $id_poliza_xml = $this->request->getPost('id_poliza_xml'); 

            $DePolizaXmlEntity = new DePolizaXml_entity();
            $DePolizaXmlEntity->{DePolizaXml_entity::ID_POLIZA_XML} = $id_poliza_xml;
            $DePolizaXmlEntity->{DePolizaXml_entity::DESCRIPCION} = $this->request->getPost('descripcion'); 
            $id_de_poliza = $DePolizasXmlModel->insert($DePolizaXmlEntity);
            $DePolizaXmlEntity->{DePolizaXml_entity::ID} = $id_de_poliza;

            $documento = $this->request->getFile('archivo_pdf');
            if (! $documento->hasMoved()) {

                $filepath = WRITEPATH . 'uploads/' . $documento->store();
                $originalName = $documento->getClientName();
                $type = $documento->getClientMimeType();

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://iplaneacion.com.mx/dms/contabilidad_apis/documentos/archivos/guardar_documento',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => array('id_tipo_archivo' => '1','identificador_externo' => $id_de_poliza,'documento'=> new \CURLFILE($filepath,$type,$originalName)),
                    CURLOPT_HTTPHEADER => array(
                        'Cookie: ci_session=namr4cq2floh6bh4ii8pueqjh9tmp2gq'
                    )
                ));
                $response = curl_exec($curl);
                curl_close($curl);

                if($response !== false){
                    $datos_pdf = json_decode($response,true);
                    if(is_array($datos_pdf) && array_key_exists('status',$datos_pdf) && $datos_pdf['status'] == 'ok' ){
                        $DePolizaXmlEntity->{DePolizaXml_entity::ARCHIVO_PDF} = $datos_pdf['data']['identificador'];
                        $DePolizasXmlModel->save($DePolizaXmlEntity);
                    }
                }
            }


            $documento = $this->request->getFile('archivo_xml');
            if (! $documento->hasMoved()) {

                $filepath = WRITEPATH . 'uploads/' . $documento->store();
                $originalName = $documento->getClientName();
                $type = $documento->getClientMimeType();

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://iplaneacion.com.mx/dms/contabilidad_apis/documentos/archivos/guardar_documento',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => array('id_tipo_archivo' => '2','identificador_externo' => $id_de_poliza,'documento'=> new \CURLFILE($filepath,$type,$originalName)),
                    CURLOPT_HTTPHEADER => array(
                        'Cookie: ci_session=namr4cq2floh6bh4ii8pueqjh9tmp2gq'
                    )
                ));
                $response = curl_exec($curl);
                curl_close($curl);

                if($response !== false){
                    $datos_archivo_xml = json_decode($response,true);
                    if(is_array($datos_archivo_xml) && array_key_exists('status',$datos_archivo_xml) && $datos_archivo_xml['status'] == 'ok' ){

                        $id_persona = $this->session->get('id');

                        $DePersonasXmlConfigModel = new DePersonasXmlConfig_model();
                        $configuracion = $DePersonasXmlConfigModel->where([DePersonasXmlConfig_entity::ID_PERSONA=>$id_persona])->first();
                        
                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => 'https://iplaneacion.com.mx/dms/contabilidad_queretaro/asientos/api/agregar',
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => '',
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_POSTFIELDS => [
                                'clave_poliza' => $configuracion->{DePersonasXmlConfig_entity::ID_POLIZA_NOMENCLATURA},
                                'clave_poliza_fija' => $configuracion->{DePersonasXmlConfig_entity::ID_POLIZA_FIJA},
                                'tipo' => $configuracion->{DePersonasXmlConfig_entity::SUBTOTAL_TIPO},
                                'cuenta' => $configuracion->{DePersonasXmlConfig_entity::ID_CUENTA_SUBTOTAL},
                                'total' => $this->request->getPost('subtotal'),
                                'concepto' => 'SUBTOTAL',
                                'departamento' => '9',
                                'estatus' => 'POR_APLICAR',
                                'folio' => 'XML-'.$id_de_poliza,
                                'cliente_id' => $id_persona,
                                'sucursal_id' => '3'
                            ]
                        ));
                        $response = curl_exec($curl);
                        curl_close($curl);
                        $id_asiento_subtotal = null;
                        if($response !== false){
                            $datos_xml = json_decode($response,true);
                            if(is_array($datos_xml) && array_key_exists('status',$datos_xml) && $datos_xml['status'] == 'success' ){
                                $id_asiento_subtotal = $datos_xml['data']['asiento_id'];
                            }
                        }


                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => 'https://iplaneacion.com.mx/dms/contabilidad_queretaro/asientos/api/agregar',
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => '',
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_POSTFIELDS => [
                                'clave_poliza' => $configuracion->{DePersonasXmlConfig_entity::ID_POLIZA_NOMENCLATURA},
                                'clave_poliza_fija' => $configuracion->{DePersonasXmlConfig_entity::ID_POLIZA_FIJA},
                                'tipo' => $configuracion->{DePersonasXmlConfig_entity::IVA_TIPO},
                                'cuenta' => $configuracion->{DePersonasXmlConfig_entity::ID_CUENTA_IVA},
                                'total' => $this->request->getPost('iva'),
                                'concepto' => 'IVA',
                                'departamento' => '9',
                                'estatus' => 'POR_APLICAR',
                                'folio' => 'XML-'.$id_de_poliza,
                                'cliente_id' => $id_persona,
                                'sucursal_id' => '3'
                            ]
                        ));
                        $response = curl_exec($curl);
                        curl_close($curl);
                        $id_asiento_iva = null;
                        if($response !== false){
                            $datos_xml = json_decode($response,true);
                            if(is_array($datos_xml) && array_key_exists('status',$datos_xml) && $datos_xml['status'] == 'success' ){
                                $id_asiento_iva = $datos_xml['data']['asiento_id'];
                            }
                        }



                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => 'https://iplaneacion.com.mx/dms/contabilidad_queretaro/asientos/api/agregar',
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => '',
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_POSTFIELDS => [
                                'clave_poliza' => $configuracion->{DePersonasXmlConfig_entity::ID_POLIZA_NOMENCLATURA},
                                'clave_poliza_fija' => $configuracion->{DePersonasXmlConfig_entity::ID_POLIZA_FIJA},
                                'tipo' => $configuracion->{DePersonasXmlConfig_entity::TOTAL_TIPO},
                                'cuenta' => $configuracion->{DePersonasXmlConfig_entity::ID_CUENTA_TOTAL},
                                'total' => $this->request->getPost('total'),
                                'concepto' => 'TOTAL',
                                'departamento' => '9',
                                'estatus' => 'POR_APLICAR',
                                'folio' => 'XML-'.$id_de_poliza,
                                'cliente_id' => $id_persona,
                                'sucursal_id' => '3'
                            ]
                        ));
                        $response = curl_exec($curl);
                        curl_close($curl);
                        $id_asiento_total = null;
                        $id_transaccion = null;
                        $poliza_id = null;
                        if($response !== false){
                            $datos_xml = json_decode($response,true);
                            if(is_array($datos_xml) && array_key_exists('status',$datos_xml) && $datos_xml['status'] == 'success' ){
                                $id_asiento_total = $datos_xml['data']['asiento_id'];
                                $id_transaccion = $datos_xml['data']['transaccion_id'];
                                $poliza_id = $datos_xml['data']['poliza_id'];
                            }
                        }

                        $DePolizaXmlEntity->{DePolizaXml_entity::ARCHIVO_XML} = $datos_archivo_xml['data']['identificador'];
                        $DePolizaXmlEntity->{DePolizaXml_entity::TOTAL} = $this->request->getPost('total'); 
                        $DePolizaXmlEntity->{DePolizaXml_entity::IVA} = $this->request->getPost('iva'); 
                        $DePolizaXmlEntity->{DePolizaXml_entity::SUBTOTAL} = $this->request->getPost('subtotal'); 
                        $DePolizaXmlEntity->{DePolizaXml_entity::ASIENTO_ID_IVA} = $id_asiento_iva;
                        $DePolizaXmlEntity->{DePolizaXml_entity::ASIENTO_ID_SUBTOTAL} = $id_asiento_subtotal;
                        $DePolizaXmlEntity->{DePolizaXml_entity::ASIENTO_ID_TOTAL} = $id_asiento_total;
                        $DePolizaXmlEntity->{DePolizaXml_entity::ACTIVO} = 1;
                        $DePolizaXmlEntity->{DePolizaXml_entity::ID_TRANSACCION} = $id_transaccion;
                        $DePolizasXmlModel->save($DePolizaXmlEntity);

                        $CaPolizasXmlModel = new CaPolizasXml_model();
                        $CaPolizaXmlEntity = $CaPolizasXmlModel->find($id_poliza_xml);
                        $CaPolizaXmlEntity->{CaPolizaXml_entity::ID_POLIZA} = $poliza_id;
                        $CaPolizaXmlEntity->{CaPolizaXml_entity::ID_TRANSACCION} = $id_transaccion;
                        $CaPolizaXmlEntity->{CaPolizaXml_entity::ID_POLIZA_NOMENCLATURA} = $configuracion->{DePersonasXmlConfig_entity::ID_POLIZA_NOMENCLATURA};
                        $CaPolizasXmlModel->save($CaPolizaXmlEntity);                        

                    }
                }
            }


            // $formModel = new FormModel();
            // formModel->save([
            //     'name' => $this->request->getVar('name'),
            //     'email'  => $this->request->getVar('email'),
            //     'phone'  => $this->request->getVar('phone'),
            // ]);          
            // return $this->response->redirect(site_url('/submit-form'));
        }else{
            $this->format_response['status'] = 'error';
            $this->format_response['message'] = $this->validation->getErrors();
        }
        
        return $this->respond($this->format_response);

    }

    public function transaccion_guardar(){

        $rules = [
            "descripcion" => [
                "label" => "Descripción", 
                "rules" => "required|max_length[500]"
            ]
        ];

        if ($this->validate($rules)) {

            $id_poliza_xml = $this->request->getPost('identity'); 

            $CaPolizasXmlModel = new CaPolizasXml_model();
            $CaPolizaXmlEntity = $CaPolizasXmlModel->where(CaPolizaXml_entity::ID,$id_poliza_xml)->first();
            $CaPolizaXmlEntity->{CaPolizaXml_entity::DESCRIPCION} = $this->request->getPost('descripcion');
            $CaPolizaXmlEntity->{CaPolizaXml_entity::ID_ESTATUS_POLIZA_XML} = 2;
            $CaPolizaXmlEntity->{CaPolizaXml_entity::ACTIVO} = 1;
            $response = $CaPolizasXmlModel->save($CaPolizaXmlEntity);         
            $this->format_response['data'] = [
                'response' => $response
            ];

        }else{
            $this->format_response['status'] = 'error';
            $this->format_response['message'] = $this->validation->getErrors();
        }
        
        return $this->respond($this->format_response);

    }

    public function transaccion_guardar_enviar(){

        $rules = [
            "descripcion" => [
                "label" => "Descripción", 
                "rules" => "required|max_length[500]"
            ]
        ];

        if ($this->validate($rules)) {

            $id_poliza_xml = $this->request->getPost('identity'); 

            $CaPolizasXmlModel = new CaPolizasXml_model();
            $CaPolizaXmlEntity = $CaPolizasXmlModel->where(CaPolizaXml_entity::ID,$id_poliza_xml)->first();
            $CaPolizaXmlEntity->{CaPolizaXml_entity::DESCRIPCION} = $this->request->getPost('descripcion');
            $CaPolizaXmlEntity->{CaPolizaXml_entity::ID_ESTATUS_POLIZA_XML} = 3;
            $CaPolizaXmlEntity->{CaPolizaXml_entity::ACTIVO} = 1;
            $response = $CaPolizasXmlModel->save($CaPolizaXmlEntity);         
            $this->format_response['data'] = [
                'response' => $response
            ];

        }else{
            $this->format_response['status'] = 'error';
            $this->format_response['message'] = $this->validation->getErrors();
        }
        
        return $this->respond($this->format_response);

    }

    public function ver_contenido_xml(){
        $archivo = base64_decode($this->request->getPost('archivo')); 
        $xml = simplexml_load_string($archivo); 
        // $ns = $xml->getNamespaces(true);
 
        $total = 0;
        $subtotal = 0;
        $iva = 0;
        
        foreach($xml->attributes() as $a => $b) {
            if($a == 'SubTotal'){
                $subtotal = (string)$b;
            }
            if($a == 'Total'){
                $total = (string)$b;
            }
        }

        $this->format_response['data'] = [
            'iva' => $total - $subtotal,
            'subtotal' => $subtotal,
            'total' => $total,
            // 'detalle' => $xml->attributes()
        ];

        return $this->respond($this->format_response);
    }

    public function eliminar_contenido_xml(){
        $archivo = $this->request->getPost('poliza');

        $DePolizasXmlModel = new DePolizasXml_model();

        $this->format_response['data'] = [
            'delete' => $DePolizasXmlModel->delete($archivo)
        ];

        return $this->respond($this->format_response);
    }


    public function eliminar_transaccion_xml(){
        $archivo = $this->request->getPost('poliza');

        $CaPolizasXmlModel = new CaPolizasXml_model();

        $this->format_response['data'] = [
            'delete' => $CaPolizasXmlModel->delete($archivo)
        ];

        return $this->respond($this->format_response);
    }
}