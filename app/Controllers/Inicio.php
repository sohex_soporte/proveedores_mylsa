<?php

namespace App\Controllers;

use App\Core\BaseController;
use App\Libraries\Gravatar;

use App\Entities\CaPersonas_entity;

use App\Models\CaPersonas_model;

class Inicio extends BaseController
{
    public function index()
    {
        helper(['form']);
        return view('inicio/index');
    }

    public function Autenticacion()
    {
        $session = session();
        $TblPersonasModel = new CaPersonas_model();

        $rfc = $this->request->getVar('rfc');
        $contrasena = $this->request->getVar('contrasena');
        
        $data = $TblPersonasModel->where(CaPersonas_entity::RFC, $rfc)->first();
        if($data){
            $pass = $data->{CaPersonas_entity::CONTRASENA};
            $authenticatePassword = (md5($contrasena) === md5($pass))? true : false;
            if($authenticatePassword){
                
                $GravatarLib = new Gravatar();
                
                $ses_data = [
                    'id' => $data->id,
                    'nombre' => trim($data->nombre.' '.$data->apellido1.' '.$data->apellido2),
                    'correo_electronico' => $data->correo_electronico,
                    'rfc' => $data->rfc,
                    'imagen' => $GravatarLib->get_image($data->correo_electronico),
                    'isLoggedIn' => TRUE
                ];

                $session->set($ses_data);
                return redirect()->to('/proveedores');
            
            }else{
                $session->setFlashdata('msg', 'Contraseña incorrecta');
                return redirect()->to('/');
            }
        }else{
            $session->setFlashdata('msg', 'El RFC no se encuentra registrado.');
            return redirect()->to('/');
        }
    }

    public function cerrar_session(){
        $this->session->destroy();
        return redirect()->to('/');
    }

}
