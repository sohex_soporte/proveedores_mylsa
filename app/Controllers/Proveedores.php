<?php

namespace App\Controllers;

use App\Core\BaseController;

use App\Entities\CaPersonas_entity;
use App\Entities\CaTiposPersonas_entity;
use App\Entities\CaPolizaXml_entity;
use App\Entities\DePersonasXmlConfig_entity;
use App\Entities\CaPolizas_entity;

use App\Models\CaPersonas_model;
use App\Models\CaTipoPersonas_model;
use App\Models\CaPolizas_model;
use App\Models\CaPolizasXml_model;
use App\Models\VwPollizasXml_model;
use App\Models\VwPollizasXmlGeneral_model;
use App\Models\DePersonasXmlConfig_model;

use App\Libraries\Utils;

class Proveedores extends BaseController
{
    protected $helpers = ['form','html','number'];

    public function index()
    {

        $id_persona = $this->session->get('id');

        $VwPollizasXmlModel = new VwPollizasXmlGeneral_model();
        $listado = $VwPollizasXmlModel->where(['persona_id' => $id_persona])->findAll();
        // dd($listado);

        $table = new \CodeIgniter\View\Table();
        $template = [
            'table_open' => '<table class="table table-vcenter table-bordered card-table table-hover">'
        ];
        $table->setTemplate($template);

        $table->setHeading(['Fecha','Descripcion','Subtotal','iva','Total','Estatus','Acciones']);
        
        if($listado != null){
            foreach($listado as $list){
                $table->addRow([
                    '<center>'.$list->created_at->toLocalizedString('kk:mm').'<br/><small>'.$list->created_at->toLocalizedString('dd/MM/yyyy').'</small></center>',
                    $list->poliza_xml_descripcion,
                    number_to_currency($list->subtotal, 'USD', 'en_US', 2),
                    number_to_currency($list->iva, 'USD', 'en_US', 2),
                    number_to_currency($list->total, 'USD', 'en_US', 2),
                    $list->estatus_poliza_xml,
                    (($list->id_estatus_poliza_xml < 3)? '<a class="btn btn-light m-1" title="Editar" href="'.site_url('proveedores/transacciones/'.base64_encode($list->id_poliza)).'"><i class="fa-solid fa-pen-to-square"></i></a>' : '').
                    '<a class="btn btn-light m-1" title="Detalle" href="'.site_url('proveedores/transacciones_detalle/'.base64_encode($list->id_poliza)).'"><i class="fa-solid fa-list"></i></a>'.
                    (($list->id_estatus_poliza_xml < 3)? '<button onclick="app.eliminar(this);" data-poliza="'.$list->id_poliza.'" class="btn btn-light m-1" title="Eliminar"><i class="fa-solid fa-trash-can "></i></button>' : '')
                    
                ]);
            }
        }else{
            $cell = ['data' => '<center>No se encontraron comprobantes</center>', 'class' => 'highlight', 'colspan' => 6];
            $table->addRow([$cell]);
        }


        $data_view = [
            'listado' => $table->generate(),
            'id' => $id_persona
        ];

        return view('proveedores/index',$data_view);
    }

    public function transacciones($id = null)
    {
        $CaPolizasXmlModel = new CaPolizasXml_model();

        $id = @base64_decode($id);
        
        if($id != null){
            $CaPolizaXml = $CaPolizasXmlModel->where(['id_persona' => $this->session->get('id'), 'id' => $id])->First();
            if($CaPolizaXml == null){
                $id = null;
            }
        }

        if($id == null){

            $DePersonasXmlConfigModel = new DePersonasXmlConfig_model();
            $configuracion = $DePersonasXmlConfigModel->where([DePersonasXmlConfig_entity::ID_PERSONA=> $this->session->get('id') ])->first();
            
            $utils = new Utils();

            $CaPolizasEntity = new CaPolizas_entity();
            if($configuracion == null){
                $CaPolizasEntity->{CaPolizas_entity::ID_POLIZA_NOMENCLATURA} = 'B';
            }else{
                $CaPolizasEntity->{CaPolizas_entity::ID_POLIZA_NOMENCLATURA} = $configuracion->id_poliza_nomenclatura;    
            }
            $CaPolizasEntity->{CaPolizas_entity::EJERCICIO} = $utils::get_anio();
            $CaPolizasEntity->{CaPolizas_entity::MES} = $utils::get_mes();
            $CaPolizasEntity->{CaPolizas_entity::DIA} = $utils::get_dia();
            $CaPolizasEntity->{CaPolizas_entity::FECHA_CREACION} = $utils::get_DateTime();
            // dd($CaPolizasEntity);
            $CaPolizasModel = new CaPolizas_model();
            $id_poliza = $CaPolizasModel->insert($CaPolizasEntity);


            $CaPolizaXml = new CaPolizaXml_entity();
            $CaPolizaXml->{CaPolizaXml_entity::ID_PERSONA} = $this->session->get('id');
            $CaPolizaXml->{CaPolizaXml_entity::ID_POLIZA} = $id_poliza;

            $id_poliza_xml = $CaPolizasXmlModel->insert($CaPolizaXml);
            return redirect()->to('/proveedores/transacciones/'.base64_encode($id_poliza_xml));
        }


        $VwPollizasXmlModel = new VwPollizasXml_model();
        $listado = $VwPollizasXmlModel->where(['id_poliza' => $id])->findAll();
        

        $table = new \CodeIgniter\View\Table();
        $template = [
            'table_open' => '<table class="table table-vcenter table-bordered card-table table-hover">'
        ];
        $table->setTemplate($template);

        $table->setHeading(['Fecha','Descripcion','Subtotal','iva','Total','Acciones']);
        
        if($listado != null){
            foreach($listado as $list){
                $table->addRow([
                    '<center>'.$list->fecha_de_poliza_xml->toLocalizedString('kk:mm').'<br/><small>'.$list->fecha_de_poliza_xml->toLocalizedString('dd/MM/yyyy').'</small></center>',
                    $list->poliza_xml_descripcion,
                    number_to_currency($list->subtotal, 'USD', 'en_US', 2),
                    number_to_currency($list->iva, 'USD', 'en_US', 2),
                    number_to_currency($list->total, 'USD', 'en_US', 2),
                    '<button onclick="app.eliminar(this);" data-poliza="'.$list->id_poliza_xml.'" class="btn btn-light" title="Eliminar"><i class="fa-solid fa-trash-can"></i></button>'
                ]);
            }
        }else{
            $cell = ['data' => '<center>No se encontraron comprobantes</center>', 'class' => 'highlight', 'colspan' => 6];
            $table->addRow([$cell]);
        }

        $CaPolizasXmlModel = new CaPolizasXml_model();
        $CaPolizaXmlEntity = $CaPolizasXmlModel->where(CaPolizaXml_entity::ID,$id)->first();
        
        // dd($CaPolizaXmlEntity);
        $data_view = [
            'listado' => $table->generate(),
            'id' => $id,
            'poliza_xml' => $CaPolizaXmlEntity
        ];

        return view('proveedores/transacciones',$data_view);
    }

    public function transacciones_detalle($id = null)
    {
        $CaPolizasXmlModel = new CaPolizasXml_model();

        $id = @base64_decode($id);
        
        if($id != null){
            $CaPolizaXml = $CaPolizasXmlModel->where(['id_persona' => $this->session->get('id'), 'id' => $id])->First();
            if($CaPolizaXml == null){
               return redirect()->to('proveedores');
            }
        }

        
        $VwPollizasXmlModel = new VwPollizasXml_model();
        $listado = $VwPollizasXmlModel->where(['id_poliza' => $id])->findAll();
        

        $table = new \CodeIgniter\View\Table();
        $template = [
            'table_open' => '<table class="table table-vcenter card-table table-hover">'
        ];
        $table->setTemplate($template);

        $table->setHeading(['Fecha','Descripcion','Subtotal','iva','Total']);
        
        if($listado != null){
            foreach($listado as $list){
                $table->addRow([
                    '<center>'.$list->fecha_de_poliza_xml->toLocalizedString('kk:mm').'<br/><small>'.$list->fecha_de_poliza_xml->toLocalizedString('dd/MM/yyyy').'</small></center>',
                    $list->poliza_xml_descripcion,
                    number_to_currency($list->subtotal, 'USD', 'en_US', 2),
                    number_to_currency($list->iva, 'USD', 'en_US', 2),
                    number_to_currency($list->total, 'USD', 'en_US', 2)
                ]);
            }
        }else{
            $cell = ['data' => '<center>No se encontraron comprobantes</center>', 'class' => 'highlight', 'colspan' => 5];
            $table->addRow([$cell]);
        }

        $CaPolizasXmlModel = new CaPolizasXml_model();
        $CaPolizaXmlEntity = $CaPolizasXmlModel->where(CaPolizaXml_entity::ID,$id)->first();
        
        // dd($CaPolizaXmlEntity);
        $data_view = [
            'listado' => $table->generate(),
            'id' => $id,
            'poliza_xml' => $CaPolizaXmlEntity
        ];

        return view('proveedores/transacciones',$data_view);
    }

    public function comprobantes($id = null)
    {
        $CaPolizasXmlModel = new CaPolizasXml_model();

        $id = @base64_decode($id);
        
        if($id != null){
            $CaPolizaXml = $CaPolizasXmlModel->where(['id_persona' => $this->session->get('id'), 'id' => $id])->first();
            if($CaPolizaXml == null){
                return redirect()->to('/proveedores');
            }
        }

        $data_view = [
            'id' => $id
        ];

        return view('proveedores/comprobantes',$data_view);
    }


    public function detalle($id){
        
        $CaPersonasModel = new CaPersonas_model();
        $datos_proveedor = $CaPersonasModel->where(CaPersonas_entity::ID,$id)->first();

        $CaTipoPersonasModel = new CaTipoPersonas_model();
        $datos_tipo_proveedor = $CaTipoPersonasModel->where(CaTiposPersonas_entity::ID,$datos_proveedor->tipo_persona_id)->first();

        $data_view = [
            'datos_proveedor' => $datos_proveedor,
            'datos_tipo_proveedor' => $datos_tipo_proveedor
        ];

        return view('proveedores/detalle',$data_view);
    }
}
