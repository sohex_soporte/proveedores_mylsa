<?php if (!defined("BASEPATH")) exit("No direct script access allowed");

use Mifiel\ApiClient as Mifiel;
use Mifiel\Document as Doc;
use Mifiel\Certificate as Cer;

class MiFIELLib {

  protected $ci;

  public $MIFIEL_ID = MIFIEL_ID;
  public $MIFIEL_PWD = MIFIEL_PWD;
  public $MIFIEL_URL = MIFIEL_URL;

  public $connected = false;
    
  // public function __construct($certPath = NULL) {
    // $this->ci =& get_instance();
    // $this->ci->load->model('DetFirmaElectronica_model');
  // }

  /* ----------------------------------- */
  // Se separo la conexion del constructor para no estar haciendo conexion cada vez que se vaya afirmar
  // Se hace conexion solo cuando se necesita
  /* ----------------------------------- */
  public function connect() {
    if($this->connected == false){
      Mifiel::setTokens($this->MIFIEL_APPID, $this->MIFIEL_APPSECRET);
      Mifiel::url($this->MIFIEL_URL);
      $this->connected = true;
    }
  }

  function documents(){   
    $this->connect();
    $documents = [];     
    foreach (Doc::all() as $key => $value) {
      $doc = new Doc($value);
      array_push($documents, $doc->__get('values'));
    }
    return $documents;
  }

  /* ----------------------------------- */
  // METODO PARA OBTENER EL DOCUMENTO POR EL ID
  // Primero se realiza la busqueda en base de datos
  // Si no se encuentra se abre conexion, se busca el documento y se actualiza el registro en la base de datos
  /* ----------------------------------- */
  function document($id){
    
    $response = null;
    try{

      // $documento = $this->ci->DetFirmaElectronica_model->get([
      //   'id_mifiel' => $id
      // ]);
      // if(is_array($documento) && strlen($documento['fiel_widget']) > 10 ){
      //   $response = @json_decode($documento['fiel_widget']);
      //   if($response == false){
      //     $response = $this->document_find_values($id);
      //   }
      // }else{
        $response = $this->document_find_values($id);
      // }

    } catch (Exception $e) {
      log_message('error', 'trycatch MiFIEL document');
      log_message('error', $this->MIFIEL_URL);
      log_message('error', $this->MIFIEL_APPID);
      $menssage = $e->getMessage();
      log_message('error', $menssage);

      $this->ci->DetFirmaElectronica_model->update([
        'mensaje' => $menssage,
      ],[
        'id_mifiel' => $id
      ]);

    }
    return $response;
  }


  /* ----------------------------------- */
  
  /* ----------------------------------- */
  function document_download($id){
    
    $response = null;

    $this->ci->DetFirmaElectronica_model->db2->where('documento_pdf is NOT NULL', NULL, FALSE);
    $this->ci->DetFirmaElectronica_model->db2->where('documento_xml is NOT NULL', NULL, FALSE);
    $this->ci->DetFirmaElectronica_model->db2->where('cadena_firma is NOT NULL', NULL, FALSE);
    $this->ci->DetFirmaElectronica_model->db2->where('cadena_certificado is NOT NULL', NULL, FALSE);
    $documento = $this->ci->DetFirmaElectronica_model->get([
      'id_mifiel' => $id
    ]);

    if($documento == false){
      try{

          $identificador = str_replace('-','_',$id);
          $path = FCPATH."assets".DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."mifiel".DIRECTORY_SEPARATOR;
          // $path = $_SERVER["DOCUMENT_ROOT"] . "/assets/uploads/buques/AA_";

          $path_documento_firmado = $path.$identificador.".pdf"; //utils::guid();
          $path_documento_xml = $path.$identificador.".xml"; //utils::guid();

          $this->connect();
          $doc = Doc::find($id);
          $doc->saveFileSigned($path_documento_firmado);
          $doc->saveXML($path_documento_xml);

          $documento_pdf = null;
          if (@file_exists($path_documento_firmado)) {
            $documento_pdf = $path_documento_firmado;
          }

          $cadena_firma = '';
          $cadena_certificado = '';
          $documento_xml = null;
          if (@file_exists($path_documento_xml)) {
            $documento_xml = $path_documento_xml;

            
              $xml = simplexml_load_file($documento_xml);
              $firmado = $xml->signers->signer->signature; // Otengo la firma
              $cadena_firma = end($firmado);

              $certificado = base64_decode($xml->signers->signer->certificate); // Otengo el certificado
              $byte_array = unpack('C*', $certificado); // convert string to byte array
              $txt = "";
              foreach ($byte_array as $k => $v){
                $txt .= "$v,";
              }
              $cadena_certificado = substr($txt, 0, strlen($txt) - 1);	
          }

          $response = [
            'documento_pdf' => $documento_pdf,
            'documento_xml' => $documento_xml,
            'cadena_firma' => trim($cadena_firma),
            'cadena_certificado' => trim($cadena_certificado)
          ];

          $this->ci->DetFirmaElectronica_model->update($response,[
            'id_mifiel' => $id
          ]);

      } catch (Exception $e) {
        log_message('error', 'trycatch MiFIEL document_download');
        log_message('error', $this->MIFIEL_URL);
        log_message('error', $this->MIFIEL_APPID);
        $menssage = $e->getMessage();
        log_message('error', $menssage);

        $this->ci->DetFirmaElectronica_model->update([
          'mensaje' => $menssage,
        ],[
          'id_mifiel' => $id
        ]);

      }

    }else{

      $response = [
        'documento_pdf' => $documento['documento_pdf'],
        'documento_xml' => $documento['documento_xml'],
        'cadena_firma' => $documento['cadena_firma'],
        'cadena_certificado' => $documento['cadena_certificado']
      ];

    }
    return $response;
  }

  /* ----------------------------------- */
  // METODO PARA OBTENER EL DOCUMENTO POR EL ID POR EL SERVICIO DE MIFIEL
  // Se abre conexion, se busca el documento y se actualiza el registro en la base de datos
  /* ----------------------------------- */
  function document_find($id){
    $this->connect();
    $doc = new Doc(Doc::find($id));
    return $doc;
  }

  function document_find_values($id){
    $doc = $this->document_find($id);
    $response = $doc->__get('values');

    $this->ci->DetFirmaElectronica_model->update([
      'fiel_widget' => @json_encode($response),
    ],[
      'id_mifiel' => $id
    ]);
    return $response;
  }



  /* ----------------------------------- */
  // METODO PARA FIRMAR UN DOCUMENTO POR EL SERVICIO DE MIFIEL Y SE REGISTRA EL LA BASE DE DATOS
  /* ----------------------------------- */
  function createDocument(array $contenido_firma){
    $this->connect();
    $response = null;

    $id_movimiento = $this->ci->DetFirmaElectronica_model->insert([
      'servicio_firma_url' => $this->MIFIEL_URL,
      'contenido_firma' => @json_encode($contenido_firma)
    ]);

    try {
      $document = new Doc($contenido_firma);
      $document->save();
      $response = $document->__get('id');

      $this->ci->DetFirmaElectronica_model->update([
        'id_mifiel' => $response,
      ],[
        'id' => $id_movimiento
      ]);

    } catch (Exception $e) {
      log_message('error', 'trycatch MiFIEL createDocument');
      log_message('error', $this->MIFIEL_URL);
      log_message('error', $this->MIFIEL_APPID);
      $menssage = $e->getMessage();
      log_message('error', $menssage);

      $this->ci->DetFirmaElectronica_model->update([
        'mensaje' => $menssage,
      ],[
        'id' => $id_movimiento
      ]);

    }
    return $response;
  }

  function deleteDocument($id){
    $this->connect();
    Doc::delete($id);
  }

  function certificates(){
    $this->connect();
    $certificates = [];     
    foreach (Cer::all() as $key => $value) {
      $cer = new Cer($value);
      array_push($certificates, $cer->__get('values'));
    }
    return $certificates;
  }

  function certificate($id){
    $this->connect();
    $cer = new Cer(Cer::find($id));
    return $cer->__get('values');
  }

  function saveCertificate($cerPath){
    $this->connect();
    $certificate = new Cer([
      'file' => $cerPath
    ]);
    $certificate->save();

    return $certificate->__get('id');
  }

  function deleteCertificate($id){
    $this->connect();
    Cer::delete($id);
  }  
}