<?php

namespace App\Libraries;

class Utils {

    public static function now(){
        $date = new \DateTime("now", new \DateTimeZone('America/Mexico_City') );
        return $date->format('Y-m-d H:i:s');
    }

    public static function get_DateTime(){
        $date = new \DateTime("now", new \DateTimeZone('America/Mexico_City') );
        return $date->format('Y-m-d H:i:s.u');
    }

    public static function get_dia_mes_format($date = 'now'){
        $date = new \DateTime($date, new \DateTimeZone('America/Mexico_City') );
        return $date->format('d/m');
    }

    public static function get_date(){
        $date = new \DateTime("now", new \DateTimeZone('America/Mexico_City') );
        return $date->format('Y-m-d');
    }

    public static function get_anio($date = 'now'){
        $date = new \DateTime($date, new \DateTimeZone('America/Mexico_City') );
        return $date->format('Y');
    }

    public static function get_mes($date = 'now'){
        $date = new \DateTime($date, new \DateTimeZone('America/Mexico_City') );
        return $date->format('m');
    }

    public static function get_dia($date = 'now'){
        $date = new \DateTime($date, new \DateTimeZone('America/Mexico_City') );
        return $date->format('d');
    }

    public static function generate_string($input, $strength = 16) {
        $input_length = strlen($input);
        $random_string = '';
        for($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }
        return $random_string;
    }

    public static function count_decimals($x){
        return  strlen(substr(strrchr($x+"", "."), 1));
     }
     
     public static function random($min, $max){
        $decimals = max( self::count_decimals($min), self::count_decimals($max) );
        $factor = pow(10, $decimals);
        return rand($min*$factor, $max*$factor) / $factor;
     }
    
    public static function pre($arr, $exit=true) {
        echo "<pre>";
        print_r($arr);
        echo "</pre>";
        if ($exit){exit();};
    }

    public static function dump($arr, $exit=true) {
        echo "<pre>";
        var_dump($arr);
        echo "</pre>";
        if ($exit){exit();};
    }

    public static function numberPrecision($n, $precision=0, $is_round=true)
    {
        return round($n,1);
        if ($is_round) {
            $r = 5 * pow(10, -($precision+1));
            $n += (($n < 0) ? -$r : $r);
        }
           
        $comma = '.';
        
        $r = 5 * pow(10, -($precision+2));
        $n += (($n > 0) ? -$r : $r);
        $n = number_format($n, $precision+1, $comma, '');
        
        $n .= $comma;
        list($n, $frac) = explode($comma, $n, 2);
        $n = rtrim(rtrim($n, $comma) . $comma . substr($frac, 0, $precision), $comma);
        return ($n);
    }

    public static function aHora($date,$hrs = true ){
                //valida si viene vacío.
        if($date == "" or $date == "0000-00-00 00:00:00"){
                return false;
        }
        //convierte la fecha a timestamp
        $date = strtotime($date);
        return ($hrs == TRUE)? date("G:i",$date) . " hrs" : date("h:i A",$date);
            
    }

    public static function json_validator($data = null)
    {
        if (!empty($data)) {
            @json_decode($data);
            if (json_last_error() !== JSON_ERROR_NONE) {
                return false;
            }
        }
        return true;
    }

    public static function getUniqueName($extension = 'jpg'){

        $info = new \SplFileInfo($extension);
        $extension = strtolower($info->getExtension());
        
        date_default_timezone_set('UTC');
        $name = "img_";
        $name.= date("YmdHis");
        $name.= substr(md5(rand(0, PHP_INT_MAX)), 10);
        $name.= ".".$extension;
        return $name;
    }

    public static function generarContrasena($largo){
        $cadena_base =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $cadena_base .= '0123456789' ;
        //$cadena_base .= '!@#%^&*()_,./<>?;:[]{}\|=+';
        
        $password = '';
        $limite = strlen($cadena_base) - 1;
        
        for ($i=0; $i < $largo; $i++)
            $password .= $cadena_base[rand(0, $limite)];
        
        return $password;
    }

    public static function get($item){
        return (array_key_exists($item,$_GET) && strlen(trim($_GET[$item]))>0 )? $_GET[$item] : null;
    }

    public static function post($item){
        return (array_key_exists($item,$_POST) && strlen(trim($_POST[$item]))>0 )? $_POST[$item] : null;
    }

    public static function redondeo($number, $significance = 2)
    {
        return (float) round($number, $significance);
        // return (float)( is_numeric($number) && is_numeric($significance) ) ? (ceil($number/$significance)*$significance) : 0;
    }

    public static function bytesToSize($bytes, $precision = 2)
    {  
        $kilobyte = 1024;
        $megabyte = $kilobyte * 1024;
        $gigabyte = $megabyte * 1024;
        $terabyte = $gigabyte * 1024;

        if (($bytes >= 0) && ($bytes < $kilobyte)) {
            return $bytes . ' B';

        } elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
            return round($bytes / $kilobyte, $precision) . ' KB';

        } elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
            return round($bytes / $megabyte, $precision) . ' MB';

        } elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
            return round($bytes / $gigabyte, $precision) . ' GB';

        } elseif ($bytes >= $terabyte) {
            return round($bytes / $terabyte, $precision) . ' TB';
        } else {
            return $bytes . ' B';
        }
    }

    public static function encodeDate($fecha)
    {
        if(strlen($fecha) > 9){
            $fecha = str_replace('/', '-',$fecha);
            $format = 'Y-m-d';
            $d = \DateTime::createFromFormat($format, $fecha);
            if($d && $d->format($format) == $fecha){
                return $d->format('Y-m-d');
            } else {
                $format = 'd-m-Y';
                $d = \DateTime::createFromFormat($format, $fecha);
                if($d && $d->format($format) == $fecha){
                    return $d->format('Y-m-d');
                }else{
                    return false;
                }
            }
        }
        return false;
    }

    public static function decodeDate($fecha)
    {
        if(strlen($fecha) > 9){
            $fecha = str_replace('/', '-',$fecha);
            $format = 'd-m-Y';
            $d = \DateTime::createFromFormat($format, $fecha);
            if($d && $d->format($format) == $fecha){
                return $d->format('d-m-Y');
            } else {
                $format = 'Y-m-d';
                $d = \DateTime::createFromFormat($format, $fecha);
                if($d && $d->format($format) == $fecha){
                    return $d->format('d/m/Y');
                }else{
                    return false;
                }
            }
        }
        return false;
    }
    
    public static function aFecha($date,$small_format = FALSE,$tipo = false){
    
        //valida si viene vacío.
    	if(strlen(trim($date))==0 or $date == "0000-00-00 00:00:00"){return ''; }
    	
    	//convierte la fecha a timestamp
    	$date = strtotime($date);
    	
    	//extrae el mes.
    	$mes = date('m',$date);
    	
    	if($small_format){
            switch($mes){
                    case '01': $_mes = "ene"; break;
                    case '02': $_mes = "feb"; break;
                    case '03': $_mes = "mar"; break;
                    case '04': $_mes = "abr"; break;
                    case '05': $_mes = "may"; break;
                    case '06': $_mes = "jun"; break;
                    case '07': $_mes = "jul"; break;
                    case '08': $_mes = "ago"; break;
                    case '09': $_mes = "sep"; break;
                    case '10': $_mes = "oct"; break;
                    case '11': $_mes = "nov"; break;
                    case '12': $_mes = "dic"; break;
            }   	
    	}else{
            switch($mes){
                    case '01': $_mes = "Enero"; break;
                    case '02': $_mes = "Febrero"; break;
                    case '03': $_mes = "Marzo"; break;
                    case '04': $_mes = "Abril"; break;
                    case '05': $_mes = "Mayo"; break;
                    case '06': $_mes = "Junio"; break;
                    case '07': $_mes = "Julio"; break;
                    case '08': $_mes = "Agosto"; break;
                    case '09': $_mes = "Septiembre"; break;
                    case '10': $_mes = "Octubre"; break;
                    case '11': $_mes = "Noviembre"; break;
                    case '12': $_mes = "Diciembre"; break;
            }
    	}
    	
    	if($small_format){
    		//Formato YY
    		$anio = date('Y',$date);
                $mes = date('m',$date);
                $dia = date('d',$date);
    	}else{
    		//Formato YYYY
    		$anio = date('Y',$date);	
                $dia = date('j',$date);
    	}
    	
    	//forma la cadena de la fecha en formato legible.
    	if($small_format){
            $output = "$dia/$mes/$anio";
    	}else{
    		$output = "$dia de $_mes de $anio";
    	}
        if($tipo == true){
            if($tipo == "dia")
            return $dia;
            if($tipo == "anio")
            return $anio;
            else
            return false;
        }
    	
    	//retorna la fecha en formato legible.
    	return $output;

    }

    public static function aFechaHora($date,$small_format = FALSE,$tipo = false){
    
        //valida si viene vacío.
    	if(strlen(trim($date))==0 or $date == "0000-00-00 00:00:00"){return ''; }
    	
        //convierte la fecha a timestamp
        $fecha = $date;
        $separa = explode(" ",$fecha);   
        $horaFormato = date("H:i:s", strtotime($separa[1]));

    	$date = strtotime($date);
    	
    	//extrae el mes.
    	$mes = date('m',$date);
    	
    	if($small_format){
            switch($mes){
                    case '01': $_mes = "ene"; break;
                    case '02': $_mes = "feb"; break;
                    case '03': $_mes = "mar"; break;
                    case '04': $_mes = "abr"; break;
                    case '05': $_mes = "may"; break;
                    case '06': $_mes = "jun"; break;
                    case '07': $_mes = "jul"; break;
                    case '08': $_mes = "ago"; break;
                    case '09': $_mes = "sep"; break;
                    case '10': $_mes = "oct"; break;
                    case '11': $_mes = "nov"; break;
                    case '12': $_mes = "dic"; break;
            }   	
    	}else{
            switch($mes){
                    case '01': $_mes = "Enero"; break;
                    case '02': $_mes = "Febrero"; break;
                    case '03': $_mes = "Marzo"; break;
                    case '04': $_mes = "Abril"; break;
                    case '05': $_mes = "Mayo"; break;
                    case '06': $_mes = "Junio"; break;
                    case '07': $_mes = "Julio"; break;
                    case '08': $_mes = "Agosto"; break;
                    case '09': $_mes = "Septiembre"; break;
                    case '10': $_mes = "Octubre"; break;
                    case '11': $_mes = "Noviembre"; break;
                    case '12': $_mes = "Diciembre"; break;
            }
    	}
    	
    	if($small_format){
    		//Formato YY
    		    $anio = date('Y',$date);
                $mes = date('m',$date);
                $dia = date('d',$date);
    	}else{
    		//Formato YYYY
    		$anio = date('Y',$date);	
                $dia = date('j',$date);
    	}
    	
    	//forma la cadena de la fecha en formato legible.
    	if($small_format){
            $output = "$dia/$mes/$anio".' '.$horaFormato;        
    	}else{
    		$output = "$dia de $_mes de $anio".' '.$horaFormato;
    	}
        if($tipo == true){
            if($tipo == "dia")
            return $dia;
            if($tipo == "anio")
            return $anio;
            else
            return false;
        }
    	
    	//retorna la fecha en formato legible.
    	return $output;

    }
    
    public static function aFecha_slash($date,$small_format = FALSE){
    
        //valida si viene vacío.
    	if(strlen(trim($date))==0 or $date == "0000-00-00 00:00:00"){return ''; }
    	
    	//convierte la fecha a timestamp
    	$date = strtotime($date);
    	
    	//extrae el mes.
    	$mes = date('m',$date);
    	
    	if($small_format){
            switch($mes){
                    case '01': $_mes = "ene"; break;
                    case '02': $_mes = "feb"; break;
                    case '03': $_mes = "mar"; break;
                    case '04': $_mes = "abr"; break;
                    case '05': $_mes = "may"; break;
                    case '06': $_mes = "jun"; break;
                    case '07': $_mes = "jul"; break;
                    case '08': $_mes = "ago"; break;
                    case '09': $_mes = "sep"; break;
                    case '10': $_mes = "oct"; break;
                    case '11': $_mes = "nov"; break;
                    case '12': $_mes = "dic"; break;
            }   	
    	}else{
            switch($mes){
                    case '01': $_mes = "Enero"; break;
                    case '02': $_mes = "Febrero"; break;
                    case '03': $_mes = "Marzo"; break;
                    case '04': $_mes = "Abril"; break;
                    case '05': $_mes = "Mayo"; break;
                    case '06': $_mes = "Junio"; break;
                    case '07': $_mes = "Julio"; break;
                    case '08': $_mes = "Agosto"; break;
                    case '09': $_mes = "Septiembre"; break;
                    case '10': $_mes = "Octubre"; break;
                    case '11': $_mes = "Noviembre"; break;
                    case '12': $_mes = "Diciembre"; break;
            }
    	}
    	
    	if($small_format){
    		//Formato YY
    		$anio = date('Y',$date);
                $mes = date('m',$date);
                $dia = date('d',$date);
    	}else{
    		//Formato YYYY
    		$anio = date('Y',$date);	
                $dia = date('j',$date);
    	}
    	
    	//forma la cadena de la fecha en formato legible.
    	if($small_format){
    		$output = "$dia/$mes/$anio";
    	}else{
    		$output = "$dia de $_mes de $anio";
    	}
    	
    	//retorna la fecha en formato legible.
    	return $output;

    }


    public static function aMes_espanol($date,$small_format = FALSE){
    
        //valida si viene vacío.
    	if(strlen(trim($date))==0 or $date == "0000-00-00 00:00:00"){return ''; }
    	
    	//convierte la fecha a timestamp
    	$date = strtotime($date);
    	
    	//extrae el mes.
    	$mes = date('m',$date);
    	
    	if($small_format){
            switch($mes){
                    case '01': $_mes = "ene"; break;
                    case '02': $_mes = "feb"; break;
                    case '03': $_mes = "mar"; break;
                    case '04': $_mes = "abr"; break;
                    case '05': $_mes = "may"; break;
                    case '06': $_mes = "jun"; break;
                    case '07': $_mes = "jul"; break;
                    case '08': $_mes = "ago"; break;
                    case '09': $_mes = "sep"; break;
                    case '10': $_mes = "oct"; break;
                    case '11': $_mes = "nov"; break;
                    case '12': $_mes = "dic"; break;
            }   	
    	}else{
            switch($mes){
                    case '01': $_mes = "Enero"; break;
                    case '02': $_mes = "Febrero"; break;
                    case '03': $_mes = "Marzo"; break;
                    case '04': $_mes = "Abril"; break;
                    case '05': $_mes = "Mayo"; break;
                    case '06': $_mes = "Junio"; break;
                    case '07': $_mes = "Julio"; break;
                    case '08': $_mes = "Agosto"; break;
                    case '09': $_mes = "Septiembre"; break;
                    case '10': $_mes = "Octubre"; break;
                    case '11': $_mes = "Noviembre"; break;
                    case '12': $_mes = "Diciembre"; break;
            }
    	}
        return strtoupper($_mes);

    }

    public static function mes_espanol($monthNum){
        // setlocale(LC_ALL, 'es_ES');
        // $dateObj   = \DateTime::createFromFormat('!m', $monthNum);
        // $monthName = strftime('%B', $dateObj->getTimestamp());

        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $monthName = $meses[ (int)$monthNum -1];

        return mb_strtoupper($monthName,'utf-8');
    }

    public static function aFechaMes($date,$small_format = FALSE){
    
        //valida si viene vacío.
    	if(strlen(trim($date))==0 or $date == "0000-00-00 00:00:00"){return ''; }
    	
    	//convierte la fecha a timestamp
    	$date = strtotime($date);
    	
    	//extrae el mes.
    	$mes = date('m',$date);
    	
    
        switch($mes){
                case '01': $_mes = "Enero"; break;
                case '02': $_mes = "Febrero"; break;
                case '03': $_mes = "Marzo"; break;
                case '04': $_mes = "Abril"; break;
                case '05': $_mes = "Mayo"; break;
                case '06': $_mes = "Junio"; break;
                case '07': $_mes = "Julio"; break;
                case '08': $_mes = "Agosto"; break;
                case '09': $_mes = "Septiembre"; break;
                case '10': $_mes = "Octubre"; break;
                case '11': $_mes = "Noviembre"; break;
                case '12': $_mes = "Diciembre"; break;
        }
    	
    
    	
    	//retorna la fecha en formato legible.
    	return $_mes;

    }
   
    
    public static function cfechaexcel($fecha ){
        if(strlen(trim($fecha))== 0){ return FALSE; }
        $buscar = array("/","'");
        $reemplazar = array("-"," ");
        $fecha = str_replace($buscar,$reemplazar, $fecha);
       //formato fecha americana
        $fecha=date("Y-m-d",strtotime($fecha));
        //El nuevo valor de la variable: $fecha2="20-10-2008"
        return $fecha;
   }
   public static function cfecha($fecha ){
        
        if(strlen(trim($fecha))== 0){ return FALSE; }
        $fecha = str_replace('/', '-', $fecha);        
       //formato fecha americana
        $fecha = date("Y-m-d",strtotime($fecha));
        //El nuevo valor de la variable: $fecha2="20-10-2008"
        return $fecha;
   }
   public static function cfechaHora($fecha ){
        
        if(strlen(trim($fecha))== 0){ return FALSE; }
        $fecha = str_replace('/', '-', $fecha);   
        $separafecha = explode(" ",$fecha);     
       //formato fecha americana
        $fechaFormato = date("Y-m-d",strtotime($separafecha[0]));
        $hora = $separafecha[1].' '.$separafecha[2];
        $horaFormato = date("H:i:s", strtotime($hora));
        $FormatoFechaHora = $fechaFormato.' '.$horaFormato;
        //El nuevo valor de la variable: $fecha2="20-10-2008"
        return $FormatoFechaHora;
   }
   
   
   
    public static function aMes($mes) {
		switch($mes){
			case '01': $_mes = "Enero"; break;
			case '02': $_mes = "Febrero"; break;
			case '03': $_mes = "Marzo"; break;
			case '04': $_mes = "Abril"; break;
			case '05': $_mes = "Mayo"; break;
			case '06': $_mes = "Junio"; break;
			case '07': $_mes = "Julio"; break;
			case '08': $_mes = "Agosto"; break;
			case '09': $_mes = "Septiembre"; break;
			case '10': $_mes = "Octubre"; break;
			case '11': $_mes = "Noviembre"; break;
			case '12': $_mes = "Diciembre"; break;
		}
		return $_mes;
    }
    
    
    public static function construir_nombre($min=4, $max=8){
        $nombre = '';
        $vocales = array("a", "e", "i", "o", "u");
        $consonantes = array("b", "c", "d", "f", "g", "j", "l", "m", "n", "p", "r", "s", "t");
        $random_nombre = rand($min, $max);//largo de la palabra
        $random = rand(0,1);//si empieza por vocal o consonante
        for($j=0;$j<$random_nombre;$j++){//palabra
                switch($random){
                        case 0: $random_vocales = rand(0, count($vocales)-1); $nombre.= $vocales[$random_vocales]; $random = 1; break;
                        case 1: $random_consonantes = rand(0, count($consonantes)-1); $nombre.= $consonantes[$random_consonantes]; $random = 0; break;
                }
        }
        return $nombre;
    }      
    
    public static function string($num = 1){
        $i = 0;
        $strings = '';
        while ($i < $num){
            $strings .= self::construir_nombre().' ';
            $i ++;
        }
        return $strings;
    }

    public static function format($number,$tipo = 'moneda'){
        $valor = '';
        switch ($tipo) {
            case 'decimal':
                $valor = '$'.self::formatMoney($number,2);
                break;

            case 'moneda':
                $valor = ($number <> 0)? '$'.self::formatMoney($number,2) : '<center>-</center>';
                break;

            case 'porcentaje':
                $valor = self::numberPrecision($number,2).'%';
                break;
            
            default:
                $valor = $number;
                break;
        }
        return $valor;

    }
    
    public static function formatMoney($number, $fractional=false) { 
        $money = '';
        
        if ($fractional) { 
            $number = sprintf('%.2f', $number); 
        } 
        while (true) { 
            $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number); 
            if ($replaced != $number) { 
                $number = $replaced; 
            } else { 
                break; 
            } 
        }
        if($number != '0.00'){
            $money = $number; 
        }else{
            $money = '0.00';
        }

        return $money; 
    } 

    public static function truncar_decimales($number, $digitos = 2 )
    {
        $raiz = 10;
        $multiplicador = pow ($raiz,$digitos);
        $resultado = ((int)($number * $multiplicador)) / $multiplicador;
        return number_format($resultado, $digitos,'.', '');
    }
    
    function fecha($fecha) //cambia de ao/mes/dia -> dia/mes/ao
    {
        $fecha=str_replace ( "-","/",$fecha);
        $fe=explode("/",$fecha);
        $di=$fe[2];
        $me=$fe[1];
        $an=$fe[0];
        $fec=$di."/".$me."/".$an;
        return $fec;
    }

    function mes($mesi)
    {
        $mesi=$mesi-1;
        $mes[0]="Enero"; $mes[6]="Julio";
        $mes[1]="Febrero"; $mes[7]="Agosto";
        $mes[2]="Marzo"; $mes[8]="Setiembre";
        $mes[3]="Abril"; $mes[9]="Octubre";
        $mes[4]="Mayo"; $mes[10]="Noviembre";
        $mes[5]="Junio"; $mes[11]="Diciembre";

        return $mes[$mesi];
    }

    function fechalarga ($fecha)
    {
        $fecha = explode(' ', $fecha);
        $fecha = current($fecha);
        
        if(strlen(trim($fecha))==0 or $fecha == "0000-00-00 00:00:00"){return false; }
        
        $dia[0]="Domingo";
        $dia[1]="Lunes";
        $dia[2]="Martes";
        $dia[3]="Miércoles";
        $dia[4]="Jueves";
        $dia[5]="Viernes";
        $dia[6]="Sábado";
        $fecha=str_replace ( "-","/",$fecha);
        $fe=explode("/",$fecha);
        $di=$fe[2];
        $me=$fe[1];
        $nmes=  self::mes($me);
        $an=$fe[0];
        $fec=mktime ( 0, 0, 0,$me,$di,$an);
        
        $larga=$dia[ date('w',$fec) ]." ".$di." de ".$nmes." de ".$an;
        return $larga;
    }
    
    public static function avance($avance = 0){
        if($avance < 10){
            return 'Iniciada';
        }elseif($avance >= 10 && $avance <= 90){
            return 'En proceso';
        }else{
            return 'Final';
        }
        
    }
    // public static function xml2array($xml) 
    // {
	//   $arr = array();
	//   foreach ($xml as $element) {
	//     $tag = $element->getName();
	//     $e = get_object_vars($element);
		
	//     if (!empty($e)) {
	//       $arr[$tag] = $element instanceof SimpleXMLElement ? utils::xml2array($element) : $e;
	//     }
	//     else {
	//       $arr[$tag] = trim($element);
	//     }
	//   }
	//   return $arr;
	// }
    public static function textoNombre($txt){
        if($txt != false){
           //return mb_convert_encoding(mb_convert_case($txt, MB_CASE_TITLE), "UTF-8");  
           return $txt;  
            //return ucwords(strtolower($txt));
        }
        return false;
        
    }
    
    
    /***************************************************************************
     * CREADOR DE GUID
     ***************************************************************************/
    public static function guid(){
        if (function_exists('com_create_guid')){
            return trim(com_create_guid(),'{}');
        }else{
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = //chr(123)// "{"
                    substr($charid, 0, 8).$hyphen
                    .substr($charid, 8, 4).$hyphen
                    .substr($charid,12, 4).$hyphen
                    .substr($charid,16, 4).$hyphen
                    .substr($charid,20,12);
                    //.chr(125);// "}"
            return $uuid;
        }
    }
    
    public static function mayusculas($string) {
        return @strtr(strtoupper($string),"àáâãäåæçèéêëìíîïðñòóôõöøùüú", "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÜÚ");
    }
    
    public static function isAjax(){
        //return TRUE;
        return (((!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') )? true : false);
    }
    
    public static function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    public static function random_color() {
        return self::random_color_part() . self::random_color_part() . self::random_color_part();
    }
    public static function folio($input,$size = 11){
        return str_pad($input, $size, "0", STR_PAD_LEFT);
    }
    public static function folioNo($input){
        return str_pad($input, 4, "0", STR_PAD_LEFT);
    }
    public static function NoOrden($input,$anio){
        return str_pad($input, 4, "0", STR_PAD_LEFT)."/".$anio;
    }
 
    // public static function toMoney($val,$symbol='$ ',$r=2) {
    //     $n = $val; 
    //     $c = is_float($n) ? 1 : number_format($n,$r);
    //     $d = '.';
    //     $t = ',';
    //     $sign = ($n < 0) ? '-' : '';
    //     $i = $n=number_format(abs($n),$r); 
    //     $j = @(($j = $i.length) > 3) ? $j % 3 : 0; 

    //    return  @$symbol.$sign .($j ? substr($i,0, $j) + $t : '').preg_replace('/(\d{3})(?=\d)/',"$1" + $t,substr($i,$j)) ;
    // }

    public static function saveMoney($money) {
        return str_replace([',','-'], ['',''], $money);
    }

    public static function api_get(array $params){
        
        $url = $params['url'];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',

            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FOLLOWLOCATION => 1
        ));

        $response = curl_exec($curl);
        if (!curl_errno($curl)) {
            $response = @json_decode($response,true);
        }
        curl_close($curl);
        return $response;
    }

    public static function api_post(array $params){
        
        $url = $params['url'];
        $fields = $params['params'];

        $fields = array('field1' => 'valor1', 'field2' => urlencode('valor 2'));
        $fields_string = http_build_query($fields);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            // CURLOPT_RETURNTRANSFER => true,
            // CURLOPT_ENCODING => '',
            // CURLOPT_MAXREDIRS => 10,
            // CURLOPT_TIMEOUT => 0,
            // CURLOPT_FOLLOWLOCATION => true,
            // CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $fields_string,

            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FOLLOWLOCATION => 1
        ));
        $data_return = false;
        $response = curl_exec($curl);
        if (!curl_errno($curl)) {
            $data_return = @json_decode($response,true);
        }else{
            log_message('error', curl_error($curl));
        }
        curl_close($curl);
        return $data_return;
    }

    public static function api_post_json(array $params){
        
        $url = $params['url'];
        $fields = $params['params'];
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $fields,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: text/plain'
            ),
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FOLLOWLOCATION => 1
        ));

        $data_return = false;
        $response = curl_exec($curl);

        if (curl_errno($curl)) {
            log_message('error', curl_error($curl));
            $data_return = @json_decode($response, true);
        }else{
            $data_return = @json_decode($response, true);
        }
        curl_close($curl);
        return $data_return;
    }

    public static function get_nombre_dia($fecha){
        $fechats = strtotime($fecha); //pasamos a timestamp
     
        //el parametro w en la funcion date indica que queremos el dia de la semana
        //lo devuelve en numero 0 domingo, 1 lunes,....
        switch (date('w', $fechats)){
            case 0: return "Domingo"; break;
            case 1: return "Lunes"; break;
            case 2: return "Martes"; break;
            case 3: return "Miercoles"; break;
            case 4: return "Jueves"; break;
            case 5: return "Viernes"; break;
            case 6: return "Sabado"; break;
        }
     }


   
}
//===============================================