<?php $this->extend('layouts/main'); ?>

<?php $this->section('title') ?>
    Agregar nuevo comprobante
<?php $this->endSection(); ?>

<?php $this->section('content') ?>
<div class="card">
    <div class="card-body">
        <form id="form_content" method="post" action="<?php echo site_url('proveedores/comprobantes') ?>">
            <div class="row">
                <div class="col-12">
                    <div class="mb-3">
                        <label class="form-label">Descripción del comprobante</label>
                        <textarea class="form-control" name="descripcion" id="descripcion" rows="3" placeholder="Descripción" ></textarea>
                        <small class="text-danger" id="msg_descripcion"></small>
                    </div>
                </div>
                <div class="col-12">
                    <div class="mb-3">
                        <label class="form-label">Añadir archivo PDF</label>
                        <input name="archivo_pdf" id="archivo_pdf" type="file" class="form-control" accept="application/pdf" />
                        <small class="text-danger" id="msg_archivo_pdf"></small>
                    </div>
                </div>
                <div class="col-12">
                    <div class="mb-3">
                        <label class="form-label">Añadir archivo XML</label>
                        <input name="archivo_xml" id="archivo_xml" type="file" class="form-control" accept="application/xml" onchange="app.leer_xml()" />
                        <small class="text-danger" id="msg_archivo_xml"></small>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-4">
                    <div class="mb-3">
                        <label class="form-label">Subtotal</label>
                        <input name="subtotal_2" id="subtotal_2" type="text" class="form-control" disabled />
                        <input name="subtotal" id="subtotal" type="hidden" class="form-control"  />
                        <small class="text-danger" id="msg_subtotal"></small>
                    </div>
                </div>
                <div class="col-4">
                    <div class="mb-3">
                        <label class="form-label">IVA</label>
                        <input name="iva_2" id="iva_2" type="text" class="form-control" disabled />
                        <input name="iva" id="iva" type="hidden" class="form-control"  />
                        <small class="text-danger" id="msg_iva"></small>
                    </div>
                </div>
                <div class="col-4">
                    <div class="mb-3">
                        <label class="form-label">Total</label>
                        <input name="total_2" id="total_2" type="text" class="form-control" disabled />
                        <input name="total" id="total" type="hidden" class="form-control"  />
                        <small class="text-danger" id="msg_total"></small>
                    </div>
                </div>
            </div>

        </form>
    </div>
    <div class="card-footer">
        <div class="row align-items-center">
            <div class="col-12">
              
                <a href="<?php echo site_url('proveedores/transacciones/'.base64_encode($id)) ?>" class="btn btn-secondary m-1">
                    <i class="fa-solid fa-chevron-left"></i>&nbsp; Regresar</a>

                <button type="button" onclick="app.guardar()" class="btn btn-primary float-end">
                    <i class="fa-solid fa-save"></i>&nbsp; Guardar
                </button>
            </div>
        </div>
    </div>
</div>
<?php $this->endSection(); ?>

<?php $this->section('scripts') ?>
<script>
    var identity = "<?php echo $id; ?>";
</script>
<?php echo script_tag('public/assets/scripts/proveedores/comprobantes.js'); ?>
<?php $this->endSection(); ?>