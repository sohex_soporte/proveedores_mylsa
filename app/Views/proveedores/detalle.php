<?php $this->extend('layouts/main'); ?>

<?php $this->section('title') ?>
Datos del proveedor
<?php $this->endSection(); ?>

<?php $this->section('content') ?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="mb-3">
                    <label class="form-label">Nombre</label>
                    <input type="text" class="form-control" name="" value="<?php echo $datos_proveedor->nombre; ?>" disabled="">
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3">
                    <label class="form-label">Primer apellido</label>
                    <input type="text" class="form-control" name="" value="<?php echo $datos_proveedor->apellido1; ?>" disabled="">
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3">
                    <label class="form-label">Segundo apellido</label>
                    <input type="text" class="form-control" name="" value="<?php echo $datos_proveedor->apellido2; ?>" disabled="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="mb-3">
                    <label class="form-label">RFC</label>
                    <input type="text" class="form-control" name="" value="<?php echo $datos_proveedor->rfc; ?>" disabled="">
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3">
                    <label class="form-label">Teléfono</label>
                    <input type="text" class="form-control" name="" value="<?php echo $datos_proveedor->telefono; ?>" disabled="">
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3">
                    <label class="form-label">Correo electrónico</label>
                    <input type="text" class="form-control" name="" value="<?php echo $datos_proveedor->correo_electronico; ?>" disabled="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="mb-3">
                    <label class="form-label">Codigo postal</label>
                    <input type="text" class="form-control" name="" value="<?php echo $datos_proveedor->codigo_postal; ?>" disabled="">
                </div>
            </div>
            <div class="col-8">
                <div class="mb-3">
                    <label class="form-label">Domicilio</label>
                    <textarea class="form-control" name="example-textarea-input" rows="3" disabled="" ><?php echo $datos_proveedor->domicilio; ?></textarea>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="card-footer">
        <div class="row align-items-center">
            <div class="col-auto float-left">
                <a href="<?php echo site_url('proveedores') ?>" class="btn btn-secondary">
                    <i class="fa-solid fa-chevron-left"></i>&nbsp; Regresar
                </a>
            </div>
        </div>
    </div> -->
</div>
<?php $this->endSection(); ?>

