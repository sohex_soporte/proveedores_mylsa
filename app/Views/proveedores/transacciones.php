<?php $this->extend('layouts/main'); ?>

<?php $this->section('title') ?>
Factura
<?php $this->endSection(); ?>

<?php $this->section('content') ?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="mb-3">
                    <label class="form-label">Descripción</label>
                    <?php if($poliza_xml->id_estatus_poliza_xml < 3){ ?>
                        <textarea class="form-control" name="descripcion" rows="3" placeholder="Descripción"><?php echo $poliza_xml->descripcion; ?></textarea>
                    <?php }else{ ?>
                        <textarea class="form-control" disabled readonly rows="3" placeholder="Descripción"><?php echo $poliza_xml->descripcion; ?></textarea>
                    <?php } ?>
                    <small class="text-danger" id="msg_descripcion"></small>
                </div>
            </div>
            <div class="col-12">
                <div class="mb-3">
                    <label class="form-label">Comprobantes</label>
                    <div class="row">
                        <div class="col-12">
                            <?php if($poliza_xml->id_estatus_poliza_xml < 3){ ?>
                            <div class="mb-3">
                                <a href="<?php echo site_url('proveedores/comprobantes/'.base64_encode($id)); ?>"
                                    class="btn btn-info btn-square float-end">
                                    Agregar nuevo comprobante
                                </a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="table-responsive mb-3 v-100">
                                <?php echo $listado; ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <div class="card-footer">
        <div class="row align-items-center">
            <div class="col-12">
                <a href="<?php echo site_url('proveedores') ?>" class="btn btn-secondary m-1">
                    <i class="fa-solid fa-chevron-left"></i>&nbsp; Regresar
                </a>
                <?php if($poliza_xml->id_estatus_poliza_xml < 3){ ?>
                <button type="button" onclick="app.guardar_enviar()" class="btn btn-primary float-end m-1">
                    <i class="fa-solid fa-paper-plane"></i>&nbsp; Guardar y enviar
                </button>

                <button type="button" onclick="app.guardar()" class="btn btn-primary float-end m-1">
                    <i class="fa-solid fa-save"></i>&nbsp; Guardar
                </button>
                <?php } ?>

            </div>



        </div>
    </div>
</div>
<?php $this->endSection(); ?>

<?php $this->section('scripts') ?>
<script>
    var identity = "<?php echo $id; ?>";
</script>
<?php echo script_tag('public/assets/scripts/proveedores/transacciones.js'); ?>
<?php $this->endSection(); ?>