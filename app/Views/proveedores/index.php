<?php $this->extend('layouts/main'); ?>

<?php $this->section('title') ?>
Listado de facturas
<?php $this->endSection(); ?>

<?php $this->section('content') ?>
<div class="card">
    <div class="card-body">

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="mb-3">
                            <a href="<?php echo site_url('proveedores/transacciones'); ?>" class="btn btn-info btn-square float-end">
                                Agregar nueva factura
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-12">
                        <div class="table-responsive mb-3 v-100">
                            <?php echo $listado; ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endSection(); ?>

<?php $this->section('scripts') ?>
<script>
    var identity = "<?php echo $id; ?>";
</script>
<?php echo script_tag('public/assets/scripts/proveedores/index.js'); ?>
<?php $this->endSection(); ?>