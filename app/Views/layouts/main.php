<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Proveedores</title>
    <!-- CSS files -->
    <?php
        echo link_tag('public/assets/theme/dist/css/tabler.min.css');
        echo link_tag('public/assets/theme/dist/css/tabler.min.css');
        echo link_tag('public/assets/theme/dist/css/tabler-flags.min.css');
        echo link_tag('public/assets/theme/dist/css/tabler-payments.min.css');
        echo link_tag('public/assets/theme/dist/css/tabler-vendors.min.css');
        echo link_tag('public/assets/theme/dist/css/demo.min.css');
    ?>
    <style>
      body { 
        background-image: url("<?php echo base_url('public/assets/images/background-main.png'); ?>");
      }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  </head>
  <body >
    <div class="page">
    <header class="navbar navbar-expand-md navbar-light d-print-none">
        <div class="container-xl">
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-menu">
            <span class="navbar-toggler-icon"></span>
          </button>
          <h1 class="navbar-brand navbar-brand-autodark d-none-navbar-horizontal pe-0 pe-md-3">
            <a href="#">
              <?php 
                $imageProperties = [
                  'src'    => 'public/assets/images/logo_mylsa.png',
                  'alt'    => 'Proveedores',
                  'class'  => 'navbar-brand-image',
                  // 'width'  => '110',
                  'height' => '32',
                  'style' => 'height: 3.5rem !important;',
                  'title'  => 'Proveedores',
                ];
                echo img($imageProperties);
              ?>
            </a>
          </h1>
          <div class="navbar-nav flex-row order-md-last">
           
            <div class="d-none d-md-flex">
              <a href="<?php echo site_url('inicio/cerrar_session'); ?>" class="nav-link px-0 hide-theme-dark ml-2 mr-2" title="Enable dark mode" data-bs-toggle="tooltip" data-bs-placement="bottom">
                <i class="fa-solid fa-arrow-right-from-bracket fa-xl"></i>
              </a>

            </div>
            <div class="nav-item dropdown">
              <a href="#" class="nav-link d-flex lh-1 text-reset p-0" data-bs-toggle="dropdown" aria-label="Open user menu">
                <span class="avatar avatar-sm" style="background-image: url(<?php echo session()->imagen; ?>)"></span>
                <div class="d-none d-xl-block ps-2">
                  <div><?php echo session()->nombre; ?></div>
                  <div class="mt-1 small text-muted"><?php echo session()->rfc; ?></div>
                </div>
              </a>
              <div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                <a href="<?php echo site_url('proveedores/detalle/'.session()->id); ?>" class="dropdown-item">Mis datos</a>
                <div class="dropdown-divider"></div>
                <a href="<?php echo site_url('inicio/cerrar_session'); ?>" class="dropdown-item">Cerrar sessión</a>
              </div>
            </div>
          </div>
        </div>
      </header>
      <div class="navbar navbar-expand-md  ">
        <div class="collapse navbar-collapse" id="navbar-menu">
          <div class="navbar navbar-dark d-print-none" style="background: #056dae" >
            <div class="container-xl">
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo site_url('proveedores'); ?>" >
                    <span class="nav-link-icon d-md-none d-lg-inline-block">
                      <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><polyline points="5 12 3 12 12 3 21 12 19 12" /><path d="M5 12v7a2 2 0 0 0 2 2h10a2 2 0 0 0 2 -2v-7" /><path d="M9 21v-6a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v6" /></svg>
                    </span>
                    <span class="nav-link-title">
                      Inicio
                    </span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo site_url('proveedores/detalle/'.session()->id); ?>" >
                    <span class="nav-link-icon d-md-none d-lg-inline-block">
                      <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M14 3v4a1 1 0 0 0 1 1h4" /><path d="M17 21h-10a2 2 0 0 1 -2 -2v-14a2 2 0 0 1 2 -2h7l5 5v11a2 2 0 0 1 -2 2z" /><line x1="9" y1="9" x2="10" y2="9" /><line x1="9" y1="13" x2="15" y2="13" /><line x1="9" y1="17" x2="15" y2="17" /></svg>
                    </span>
                    <span class="nav-link-title">
                      Mis datos
                    </span>
                  </a>
                </li>
              </ul>
             
            </div>
          </div>
        </div>
      </div>
      <div class="page-wrapper">
        <div class="container-xl">

        <div class="page-header d-print-none">
            <div class="row align-items-center">
              <div class="col">
                <h2 class="page-title">
                <?php $this->renderSection('title'); ?>
                </h2>
              </div>
            </div>
          </div>

          <div class="page-body">
            <?php $this->renderSection('content'); ?>
        </div>


        </div>

        
        <footer class="footer footer-transparent d-print-none">
          <div class="container-xl">
            <div class="row text-center align-items-center flex-row-reverse">
              <div class="col-12 mt-3 mt-lg-0">
                <ul class="list-inline list-inline-dots mb-0">
                  <li class="list-inline-item">
                    <center>Copyright SOHEX <?php echo date('Y'); ?> - Desarrollado por SISTEMAS OPERATIVOS HEXADECIMAL. SA DE CV.</center>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- Libs JS -->
    <!-- Tabler Core -->
    <script>
      var PATH = "<?php echo site_url('/'); ?>";
    </script>
    <?php
      echo script_tag(['src'  => 'public/assets/libraries/jquery/jquery-3.6.0.js']);
      
      echo script_tag(['src'  => 'public/assets/theme/dist/js/tabler.min.js']);
      echo script_tag(['src'  => 'public/assets/theme/dist/js/demo.min.js']);
      echo script_tag(['src'  => 'public/assets/libraries/jquery-loading-overlay-master/src/loadingoverlay.js']);

      ?><script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script><?php

      $this->renderSection('scripts');

    ?>
    <script>
      $(document).ready(function () {
          $("body").LoadingOverlay("show");
          // Hide it after 3 seconds
          setTimeout(function () {
              $.LoadingOverlay("hide");
          }, 1000);
          $.ajaxSetup({
              beforeSend: function () {
                  $("body").LoadingOverlay("show");
                  setTimeout(function () {
                      $("body").LoadingOverlay("hide");
                  }, 7000)
              },
              complete: function () {
                  $("body").LoadingOverlay("hide");
              }
          });
      });
    </script>
    
    
  </body>
</html>