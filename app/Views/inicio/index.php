<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Inicio</title>
    <!-- CSS files -->
    <?php
    echo link_tag('public/assets/theme/dist/css/tabler.min.css');
    echo link_tag('public/assets/theme/dist/css/tabler-flags.min.css');
    echo link_tag('public/assets/theme/dist/css/tabler-payments.min.css');
    echo link_tag('public/assets/theme/dist/css/tabler-vendors.min.css');
    echo link_tag('public/assets/theme/dist/css/demo.min.css');
    ?>
    <style>
      body { 
        background-image: url("<?php echo base_url('public/assets/images/background-main.png'); ?>");
      }
    </style>
</head>

<body class=" border-top-wide border-primary d-flex flex-column">
    <div class="page page-center">
        <div class="container-tight py-4">

          <!-- <a href="#" class="navbar-brand navbar-brand-autodark">
              <img src="https://iplaneacion.com.mx/dms/contabilidad_queretaro/assets/img/dmslogo.png" alt="" height="60">
             
            </a> -->
           
           

            
        
            <div class="text-center mb-4">
                <a href="." class="navbar-brand navbar-brand-autodark"><img src="<?php echo base_url('public/assets/images/logo_ford.png'); ?>" height="60"
                        alt=""></a>
            </div>
            <?php if(session()->getFlashdata('msg')):?>
                    <div class="alert alert-warning">
                       <?= session()->getFlashdata('msg') ?>
                    </div>
                <?php endif;?>
            <form action="<?php echo site_url(); ?>" method="post" class="card card-md" autocomplete="off" >
                <div class="card-body">
                    <h2 class="card-title text-center mb-4">Ingrese a su cuenta</h2>
                    <div class="mb-3">
                        <label class="form-label">RFC</label>
                        <input type="text" name="rfc" placeholder="RFC" value="<?= set_value('rfc') ?>"
                            class="form-control">

                    </div>
                    <div class="mb-2">
                        <label class="form-label">
                            Contraseña
                            <!-- <span class="form-label-description">
                                <a href="./forgot-password.html">I forgot password</a>
                            </span> -->
                        </label>
                        <div class="input-group input-group-flat">
                            <input type="password" class="form-control" name="contrasena" placeholder="contrasena"
                                autocomplete="off">
                            <span class="input-group-text">
                                <a href="#" class="link-secondary" title="Mostrar contraseña" data-bs-toggle="tooltip">
                                    <!-- Download SVG icon from http://tabler-icons.io/i/eye -->
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                                        viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                        stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                        <circle cx="12" cy="12" r="2" />
                                        <path
                                            d="M22 12c-2.667 4.667 -6 7 -10 7s-7.333 -2.333 -10 -7c2.667 -4.667 6 -7 10 -7s7.333 2.333 10 7" />
                                        </svg>
                                </a>
                            </span>
                        </div>
                    </div>
                    <div class="form-footer">
                        <button type="submit" class="btn btn-primary w-100">Iniciar sesión</button>
                    </div>
                </div>

            </form>
            <div class="text-center mb-4">
                <a href="." class="navbar-brand navbar-brand-autodark"><img src="<?php echo base_url('public/assets/images/logo_mylsa.png'); ?>" height="60"
                        alt=""></a>
            </div>

        </div>
    </div>

    <?php
    echo script_tag(['src'  => 'public/assets/theme/dist/js/tabler.min.js']);
    echo script_tag(['src'  => 'public/assets/theme/dist/js/demo.min.js']);
    ?>
</body>

</html>