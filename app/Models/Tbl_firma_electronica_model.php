<?php

namespace App\Models;

use App\Entities\Tbl_firma_electronica;

use Michalsn\Uuid\UuidModel;

class Tbl_firma_electronica_model extends UuidModel
{

        protected $DBGroup = 'default';

        protected $table      = Tbl_firma_electronica::TABLE_NAME;
        
        protected $primaryKey = Tbl_firma_electronica::ID;

        protected $returnType = Tbl_firma_electronica::class;
        protected $useSoftDeletes = true;

        protected $allowedFields = [
                Tbl_firma_electronica::ID_MIFIEL,
                Tbl_firma_electronica::COMPLETO,
                Tbl_firma_electronica::CONTENIDO_FIRMA,
                Tbl_firma_electronica::CADENA_CERTIFICADO,
                Tbl_firma_electronica::CADENA_FIRMA,
                Tbl_firma_electronica::DOCUMENTO_PDF,
                Tbl_firma_electronica::DOCUMENTO_PDF_FIRMADO,
                Tbl_firma_electronica::DOCUMENTO_XML
        ];

        protected $useTimestamps = true;
        protected $createdField  = Tbl_firma_electronica::CREATED_AT;
        protected $updatedField  = Tbl_firma_electronica::UPDATED_AT;
        protected $deletedField  = Tbl_firma_electronica::DELETED_AT;

        protected $uuidFields = [
                Tbl_firma_electronica::ID,
                Tbl_firma_electronica::ID_MIFIEL
        ];
        protected $uuidUseBytes = false;
}
