<?php

namespace App\Models;

use Michalsn\Uuid\UuidModel;

use App\Entities\CaPersonas_entity;


class CaPersonas_model extends UuidModel
{

    protected $DBGroup = 'default';

    protected $table      = CaPersonas_entity::TABLE_NAME;
    protected $primaryKey = CaPersonas_entity::ID;

    protected $returnType = CaPersonas_entity::class;
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        CaPersonas_entity::RFC,
        CaPersonas_entity::CONTRASENA
    ];

    protected $useTimestamps = true;
    protected $createdField  = CaPersonas_entity::CREATED_AT;
    protected $updatedField  = CaPersonas_entity::UPDATED_AT;
    protected $deletedField  = CaPersonas_entity::DELETED_AT;

    protected $uuidFields = [
        CaPersonas_entity::ID
    ];
    protected $uuidUseBytes = false;
}
