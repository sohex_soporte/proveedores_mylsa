<?php

namespace App\Models;

use CodeIgniter\Model;
use App\Entities\CaPolizaXml_entity;

class CaPolizasXml_model extends Model
{

    protected $DBGroup = 'default';

    protected $table      = CaPolizaXml_entity::TABLE_NAME;
    protected $primaryKey = CaPolizaXml_entity::ID;

    protected $returnType = CaPolizaXml_entity::class;
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        CaPolizaXml_entity::ACTIVO,
        CaPolizaXml_entity::DESCRIPCION,
        CaPolizaXml_entity::ID_PERSONA,
        CaPolizaXml_entity::ID_TRANSACCION,
        CaPolizaXml_entity::ID_POLIZA_NOMENCLATURA,
        CaPolizaXml_entity::ID_ESTATUS_POLIZA_XML,
        CaPolizaXml_entity::ID_POLIZA
    ];

    protected $useTimestamps = true;
    protected $createdField  = CaPolizaXml_entity::CREATED_AT;
    protected $updatedField  = CaPolizaXml_entity::UPDATED_AT;
    protected $deletedField  = CaPolizaXml_entity::DELETED_AT;

}
