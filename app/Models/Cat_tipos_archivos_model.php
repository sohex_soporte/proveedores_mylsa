<?php

namespace App\Models;

use App\Entities\Cat_tipos_archivos;
use CodeIgniter\Model;

class Cat_tipos_archivos_model extends Model
{
        protected $DBGroup = 'default';

        protected $table      = 'cat_tipos_archivos';
        protected $primaryKey = Cat_tipos_archivos::ID;

        protected $returnType = Cat_tipos_archivos::class;
        protected $useSoftDeletes = false;

        protected $allowedFields = [
                Cat_tipos_archivos::NOMBRE
        ];
}
