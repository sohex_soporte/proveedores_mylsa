<?php

namespace App\Models;

use App\Entities\Tbl_archivos;

use Michalsn\Uuid\UuidModel;

class Tbl_archivos_model extends UuidModel
{

        protected $DBGroup = 'default';

        protected $table      = 'tbl_archivos';
        protected $primaryKey = Tbl_archivos::ID;

        protected $returnType = Tbl_archivos::class;
        protected $useSoftDeletes = true;

        protected $allowedFields = [
                Tbl_archivos::ID_TIPO_ARCHIVO,
                Tbl_archivos::MIME_TYPE,
                Tbl_archivos::NOMBRE_ARCHIVO,
                Tbl_archivos::RUTA
        ];

        protected $useTimestamps = true;
        protected $createdField  = Tbl_archivos::CREATED_AT;
        protected $updatedField  = Tbl_archivos::UPDATED_AT;
        protected $deletedField  = Tbl_archivos::DELETED_AT;

        protected $uuidFields = [ Tbl_archivos::ID ];
        protected $uuidUseBytes = false;
}
