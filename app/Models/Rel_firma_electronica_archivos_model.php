<?php

namespace App\Models;

use App\Entities\Rel_firma_electronica_archivos;
use App\Entities\Tbl_archivos;

use Michalsn\Uuid\UuidModel;

class Rel_firma_electronica_archivos_model extends UuidModel
{

        protected $DBGroup = 'default';

        protected $table      = Rel_firma_electronica_archivos::TABLE_NAME;
        protected $primaryKey = Rel_firma_electronica_archivos::ID;

        protected $returnType = Rel_firma_electronica_archivos::class;
        protected $useSoftDeletes = true;

        protected $allowedFields = [
                Rel_firma_electronica_archivos::ID_ARCHIVO,
                Rel_firma_electronica_archivos::ID_FIRMA_ELECTRONICA
        ];

        protected $useTimestamps = true;
        protected $createdField  = Rel_firma_electronica_archivos::CREATED_AT;
        protected $updatedField  = Rel_firma_electronica_archivos::UPDATED_AT;
        protected $deletedField  = Rel_firma_electronica_archivos::DELETED_AT;

        protected $uuidFields = [ Rel_firma_electronica_archivos::ID ];
        protected $uuidUseBytes = false;

        public function join_archivos()
        {
                return [
                        'table' => Tbl_archivos::TABLE_NAME,
                        'join' => Rel_firma_electronica_archivos::TABLE_NAME . '.' . Rel_firma_electronica_archivos::ID_ARCHIVO . '=' .tbl_archivos::TABLE_NAME . '.' . tbl_archivos::ID
                ];
        }
}
