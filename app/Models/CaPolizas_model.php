<?php

namespace App\Models;

use CodeIgniter\Model;
use App\Entities\CaPolizas_entity;

class CaPolizas_model extends Model
{

    protected $DBGroup = 'default';

    protected $table      = CaPolizas_entity::TABLE_NAME;
    protected $primaryKey = CaPolizas_entity::ID;

    protected $returnType = CaPolizas_entity::class;
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        CaPolizas_entity::ID_POLIZA_NOMENCLATURA,
        CaPolizas_entity::EJERCICIO,
        CaPolizas_entity::MES,
        CaPolizas_entity::DIA,
        CaPolizas_entity::FECHA_CREACION
    ];

    protected $useTimestamps = true;
    protected $createdField  = CaPolizas_entity::CREATED_AT;
    protected $updatedField  = CaPolizas_entity::UPDATED_AT;
    protected $deletedField  = CaPolizas_entity::DELETED_AT;

}
