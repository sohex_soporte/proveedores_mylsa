<?php

namespace App\Models;

use CodeIgniter\Model;
use App\Entities\VwPollizasXml_entity;

class VwPollizasXml_model extends Model
{

    protected $DBGroup = 'default';

    protected $table      = VwPollizasXml_entity::TABLE_NAME;
    protected $primaryKey = VwPollizasXml_entity::ID;

    protected $returnType = VwPollizasXml_entity::class;
    protected $useSoftDeletes = true;

    protected $allowedFields = [
    ];

    protected $useTimestamps = true;
    protected $createdField  = VwPollizasXml_entity::CREATED_AT;
    protected $updatedField  = VwPollizasXml_entity::UPDATED_AT;
    protected $deletedField  = VwPollizasXml_entity::DELETED_AT;

}
