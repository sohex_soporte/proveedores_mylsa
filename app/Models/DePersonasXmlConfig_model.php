<?php

namespace App\Models;

use Michalsn\Uuid\UuidModel;
use App\Entities\DePersonasXmlConfig_entity;

class DePersonasXmlConfig_model extends UuidModel
{

        protected $DBGroup = 'default';

        protected $table      = DePersonasXmlConfig_entity::TABLE_NAME;
        protected $primaryKey = DePersonasXmlConfig_entity::ID;

        protected $returnType = DePersonasXmlConfig_entity::class;
        protected $useSoftDeletes = true;

        protected $allowedFields = [
            DePersonasXmlConfig_entity::ID_PERSONA
        ];

        protected $useTimestamps = true;
        protected $createdField  = DePersonasXmlConfig_entity::CREATED_AT;
        protected $updatedField  = DePersonasXmlConfig_entity::UPDATED_AT;
        protected $deletedField  = DePersonasXmlConfig_entity::DELETED_AT;

        protected $uuidFields = [
            DePersonasXmlConfig_entity::ID
        ];

        protected $uuidUseBytes = false;

}
