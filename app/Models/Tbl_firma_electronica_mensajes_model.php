<?php

namespace App\Models;

use App\Entities\Tbl_firma_electronica_mensajes;

use Michalsn\Uuid\UuidModel;

class Tbl_firma_electronica_mensajes_model extends UuidModel
{

        protected $DBGroup = 'default';

        protected $table      = 'tbl_firma_electronica_mensajes';
        protected $primaryKey = Tbl_firma_electronica_mensajes::ID;

        protected $returnType = Tbl_firma_electronica_mensajes::class;
        protected $useSoftDeletes = true;

        protected $allowedFields = [
                Tbl_firma_electronica_mensajes::ID_FIRMA_ELECTRONICA,
                Tbl_firma_electronica_mensajes::MENSAJE
        ];

        protected $useTimestamps = true;
        protected $createdField  = Tbl_firma_electronica_mensajes::CREATED_AT;
        protected $updatedField  = Tbl_firma_electronica_mensajes::UPDATED_AT;
        protected $deletedField  = Tbl_firma_electronica_mensajes::DELETED_AT;

        protected $uuidFields = [
                Tbl_firma_electronica_mensajes::ID,
                Tbl_firma_electronica_mensajes::ID_FIRMA_ELECTRONICA
        ];

        protected $uuidUseBytes = false;
}
