<?php

namespace App\Models;

use CodeIgniter\Model;
use App\Entities\DePolizaXml_entity;

class DePolizasXml_model extends Model
{

    protected $DBGroup = 'default';

    protected $table      = DePolizaXml_entity::TABLE_NAME;
    protected $primaryKey = DePolizaXml_entity::ID;

    protected $returnType = CaPolizaXml_entity::class;
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        DePolizaXml_entity::ACTIVO,
        DePolizaXml_entity::ARCHIVO_PDF,
        DePolizaXml_entity::ARCHIVO_XML,
        DePolizaXml_entity::CONCEPTO,
        DePolizaXml_entity::ASIENTO_ID_SUBTOTAL,
        DePolizaXml_entity::ASIENTO_ID_IVA,
        DePolizaXml_entity::ASIENTO_ID_TOTAL,
        DePolizaXml_entity::IVA,
        DePolizaXml_entity::TOTAL,
        DePolizaXml_entity::SUBTOTAL,
        DePolizaXml_entity::DESCRIPCION,
        DePolizaXml_entity::ID_POLIZA_XML,
        DePolizaXml_entity::ID_TRANSACCION
    ];

    protected $useTimestamps = true;
    protected $createdField  = DePolizaXml_entity::CREATED_AT;
    protected $updatedField  = DePolizaXml_entity::UPDATED_AT;
    protected $deletedField  = DePolizaXml_entity::DELETED_AT;

}
