<?php

namespace App\Models;

use CodeIgniter\Model;

use App\Entities\CaTiposPersonas_entity;


class CaTipoPersonas_model extends Model
{

    protected $DBGroup = 'default';

    protected $table      = CaTiposPersonas_entity::TABLE_NAME;
    protected $primaryKey = CaTiposPersonas_entity::ID;

    protected $returnType = CaTiposPersonas_entity::class;
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        CaTiposPersonas_entity::NOMBRE
    ];

    protected $useTimestamps = FALSE;

}
