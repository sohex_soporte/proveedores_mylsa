<?php

namespace App\Models;

use App\Entities\Tbl_firmantes;

use Michalsn\Uuid\UuidModel;

class Tbl_firmantes_model extends UuidModel
{

        protected $DBGroup = 'default';

        protected $table      = Tbl_firmantes::TABLE_NAME;
        protected $primaryKey = Tbl_firmantes::ID;

        protected $returnType = Tbl_firmantes::class;
        protected $useSoftDeletes = true;

        protected $allowedFields = [
                Tbl_firmantes::NOMBRE,
                Tbl_firmantes::CORREO_ELECTRONICO
        ];

        protected $useTimestamps = true;
        protected $createdField  = Tbl_firmantes::CREATED_AT;
        protected $updatedField  = Tbl_firmantes::UPDATED_AT;
        protected $deletedField  = Tbl_firmantes::DELETED_AT;

        protected $uuidFields = [
                Tbl_firmantes::ID
        ];
        protected $uuidUseBytes = false;
}
