<?php

namespace App\Models;

use CodeIgniter\Model;
use App\Entities\VwPollizasXmlGeneral_entity;

class VwPollizasXmlGeneral_model extends Model
{

    protected $DBGroup = 'default';

    protected $table      = VwPollizasXmlGeneral_entity::TABLE_NAME;
    protected $primaryKey = VwPollizasXmlGeneral_entity::ID;

    protected $returnType = VwPollizasXmlGeneral_entity::class;
    protected $useSoftDeletes = true;

    protected $allowedFields = [
    ];

    protected $useTimestamps = true;
    protected $createdField  = VwPollizasXmlGeneral_entity::CREATED_AT;
    protected $updatedField  = VwPollizasXmlGeneral_entity::UPDATED_AT;
    protected $deletedField  = VwPollizasXmlGeneral_entity::DELETED_AT;

}
