<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class Rel_firma_electronica_archivos extends Entity
{
        public const TABLE_NAME = 'rel_firma_electronica_archivos';

        public const ID = 'ID';
        public const ID_FIRMA_ELECTRONICA = 'id_firma_electronica';
        public const ID_ARCHIVO = 'id_archivo';

        public const CREATED_AT = 'created_at';
        public const UPDATED_AT = 'updated_at';
        public const DELETED_AT = 'deleted_at';

        protected $attributes = [
                self::ID => null,
                self::ID_FIRMA_ELECTRONICA => null,
                self::ID_ARCHIVO => null
        ];

        protected $dates = [
                self::CREATED_AT,
                self::UPDATED_AT,
                self::DELETED_AT
        ];

        protected $casts = [
                self::ID => 'string',
                self::ID_FIRMA_ELECTRONICA => 'string',
                self::ID_ARCHIVO => 'string',

                self::CREATED_AT => 'datetime',
                self::UPDATED_AT => 'datetime',
                self::DELETED_AT => '?datetime'
        ];

}
