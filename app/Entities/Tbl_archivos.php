<?php

namespace App\Entities;

use Michalsn\Uuid\UuidEntity;

class Tbl_archivos extends UuidEntity
{
        public const TABLE_NAME = 'tbl_archivos';

        public const ID = 'id';
        public const ID_TIPO_ARCHIVO = 'id_tipo_archivo';
        public const MIME_TYPE = 'mime_type';
        public const NOMBRE_ARCHIVO = 'nombre_archivo';
        public const RUTA = 'ruta';

        public const CREATED_AT = 'created_at';
        public const UPDATED_AT = 'updated_at';
        public const DELETED_AT = 'deleted_at';

        protected $attributes = [
                self::MIME_TYPE => null,
                self::RUTA => null,
                self::NOMBRE_ARCHIVO => null
        ];

        protected $dates = [
                self::CREATED_AT,
                self::UPDATED_AT,
                self::DELETED_AT
        ];

        protected $uuids = [
                self::ID
        ];

        protected $casts = [
                self::ID => 'uuid',
                self::ID_TIPO_ARCHIVO => 'integer',
                self::MIME_TYPE => '?string',
                self::RUTA => 'string',
                self::NOMBRE_ARCHIVO => '?string',

                self::CREATED_AT => 'datetime',
                self::UPDATED_AT => 'datetime',
                self::DELETED_AT => '?datetime'
        ];
}
