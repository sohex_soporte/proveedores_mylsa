<?php

namespace App\Entities;

use Michalsn\Uuid\UuidEntity;

class Tbl_firma_electronica extends UuidEntity
{
        public const TABLE_NAME = 'tbl_firma_electronica';

        public const ID = 'id';
        public const ID_MIFIEL = 'id_mifiel';
        public const CADENA_FIRMA = 'cadena_firma';
        public const CONTENIDO_FIRMA = 'contenido_firma';
        public const CADENA_CERTIFICADO = 'cadena_certificado';
        public const COMPLETO = 'completo';
        public const DOCUMENTO_PDF = 'documento_pdf';
        public const DOCUMENTO_XML = 'documento_xml';
        public const DOCUMENTO_PDF_FIRMADO = 'documento_pdf_firmado';
        public const ID_TIPO_DOCUMENTO = 'id_tipo_documento';
        public const ID_ESTATUS_FIRMA_ELECTRONICA = 'id_estatus_firma_electronica';
        public const ID_FIRMANTE = 'id_firmante';
        public const IDENTIFICADOR_EXTERNO = 'identificador_externo';

        public const CREATED_AT = 'created_at';
        public const UPDATED_AT = 'updated_at';
        public const DELETED_AT = 'deleted_at';

        protected $attributes = [
                self::ID_MIFIEL => null,
                self::CADENA_FIRMA => null,
                self::CONTENIDO_FIRMA => null,
                self::CADENA_CERTIFICADO => null,
                self::COMPLETO => 'no',
                self::ID_ESTATUS_FIRMA_ELECTRONICA => 1,
                self::ID_FIRMANTE => null,
                self::IDENTIFICADOR_EXTERNO => null
        ];

        protected $dates = [
                self::CREATED_AT,
                self::UPDATED_AT,
                self::DELETED_AT
        ];

        protected $uuids = [
                self::ID,
                self::ID_MIFIEL
        ];

        protected $casts = [
                self::ID => 'uuid',
                self::ID_MIFIEL => '?uuid',
                self::COMPLETO => '?string',

                self::CREATED_AT => 'datetime',
                self::UPDATED_AT => 'datetime',
                self::DELETED_AT => '?datetime'
        ];
}
