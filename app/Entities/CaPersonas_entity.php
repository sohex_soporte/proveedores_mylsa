<?php

namespace App\Entities;

use Michalsn\Uuid\UuidEntity;

class CaPersonas_entity extends UuidEntity
{

        public const TABLE_NAME = 'ca_personas';

        public const ID = 'id';
        public const CLIENT_ID = 'cliente_id';
        public const TIPO_PERSONA_ID = 'tipo_persona_id';
        public const NOMBRE = 'nombre';
        public const APELLIDO1 = 'apellido1';
        public const APELLIDO2 = 'apellido2';
        public const RFC = 'rfc';
        public const TELEFONO = 'telefono';
        public const DOMICILIO = 'domicilio';
        public const CORREO_ELECTRONICO = 'correo_electronico';
        public const CODIGO_POSTAL = 'codigo_postal';
        public const CONTRASENA = 'contrasena';

        public const CREATED_AT = 'created_at';
        public const UPDATED_AT = 'updated_at';
        public const DELETED_AT = 'deleted_at';

        protected $attributes = [
            self::CLIENT_ID => null,
            self::TIPO_PERSONA_ID => null,
            self::NOMBRE => null,
            self::APELLIDO1 => null,
            self::APELLIDO2 => null,
            self::RFC => null,
            self::TELEFONO => null,
            self::DOMICILIO => null,
            self::CORREO_ELECTRONICO => null,
            self::CODIGO_POSTAL => null,
            self::CONTRASENA => null
        ];

        protected $dates = [
            self::CREATED_AT,
            self::UPDATED_AT,
            self::DELETED_AT
        ];

        protected $uuids = [
            self::ID
        ];

        protected $casts = [
            self::ID => 'uuid',
            self::RFC => 'string',
            self::CONTRASENA => 'string',
            self::CREATED_AT => 'datetime',
            self::UPDATED_AT => 'datetime',
            self::DELETED_AT => '?datetime'
        ];
}
