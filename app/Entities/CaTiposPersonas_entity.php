<?php

namespace App\Entities;

use Michalsn\Uuid\UuidEntity;

class CaTiposPersonas_entity extends UuidEntity
{

        public const TABLE_NAME = 'ca_tipo_persona';

        public const ID = 'id';
        public const NOMBRE = 'nombre';

        protected $attributes = [
            self::NOMBRE => null
        ];


        protected $casts = [
            self::ID => 'int',
            self::NOMBRE => 'string'
        ];
}
