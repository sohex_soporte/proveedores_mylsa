<?php

namespace App\Entities;

use Michalsn\Uuid\UuidEntity;

class Tbl_firma_electronica_mensajes extends UuidEntity
{

        public const ID = 'id';
        public const ID_FIRMA_ELECTRONICA = 'id_firma_electronica';
        public const MENSAJE = 'mensaje';

        public const CREATED_AT = 'created_at';
        public const UPDATED_AT = 'updated_at';
        public const DELETED_AT = 'deleted_at';

        protected $attributes = [
                self::ID_FIRMA_ELECTRONICA => null,
                self::MENSAJE => null
        ];

        protected $dates = [
                self::CREATED_AT,
                self::UPDATED_AT,
                self::DELETED_AT
        ];

        protected $uuids = [
                self::ID,
                self::ID_FIRMA_ELECTRONICA
        ];

        protected $casts = [
                self::ID => 'uuid',
                self::ID_FIRMA_ELECTRONICA => 'uuid',
                self::MENSAJE => '?string',

                self::CREATED_AT => 'datetime',
                self::UPDATED_AT => 'datetime',
                self::DELETED_AT => '?datetime'
        ];
}
