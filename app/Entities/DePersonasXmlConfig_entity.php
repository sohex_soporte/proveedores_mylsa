<?php

namespace App\Entities;

use Michalsn\Uuid\UuidEntity;

class DePersonasXmlConfig_entity extends UuidEntity
{

        public const TABLE_NAME = 'de_personas_xml_config';

        public const ID = 'id';
        public const ID_PERSONA = 'id_persona';
        public const ID_CUENTA_TOTAL = 'id_cuenta_total';
        public const ID_CUENTA_IVA = 'id_cuenta_iva';
        public const ID_CUENTA_SUBTOTAL= 'id_cuenta_subtotal';
        public const ID_POLIZA_NOMENCLATURA = 'id_poliza_nomenclatura';
        public const ID_POLIZA_FIJA = 'id_poliza_fija';
        public const TOTAL_TIPO = 'total_tipo';
        public const SUBTOTAL_TIPO = 'subtotal_tipo';
        public const IVA_TIPO = 'iva_tipo';

        public const CREATED_AT = 'created_at';
        public const UPDATED_AT = 'updated_at';
        public const DELETED_AT = 'deleted_at';

        public const DEFAULT_ID = '00000000-0000-0000-0000-000000000000';

        protected $attributes = [
            self::ID_PERSONA => null,
            self::ID_CUENTA_TOTAL => null,
            self::ID_CUENTA_IVA => null,
            self::ID_CUENTA_SUBTOTAL => null,
            self::ID_POLIZA_NOMENCLATURA => null,
            self::ID_POLIZA_FIJA => null,
            self::TOTAL_TIPO => null,
            self::SUBTOTAL_TIPO => null,
            self::IVA_TIPO => null
        ];

        protected $dates = [
            self::CREATED_AT,
            self::UPDATED_AT,
            self::DELETED_AT
        ];

        protected $uuids = [
            self::ID
        ];

        protected $casts = [
            self::ID => 'integer',
            self::CREATED_AT => 'datetime',
            self::UPDATED_AT => 'datetime',
            self::DELETED_AT => '?datetime'
        ];
}
