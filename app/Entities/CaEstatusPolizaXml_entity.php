<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class CaEstatusPolizaXml_entity extends Entity
{

        public const ID = 'id';
        public const NOMBRE = 'nombre';

        public const ID_ESTATUS_PRELLENADO = 1;
        public const ID_ESTATUS_ENVIADO = 2;
        
        protected $attributes = [
                self::ID => null,
                self::NOMBRE => null
        ];

        protected $casts = [
                self::ID => 'integer',
                self::NOMBRE => 'string'
        ];

}
