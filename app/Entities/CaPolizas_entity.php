<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class CaPolizas_entity extends Entity
{

        public const TABLE_NAME = 'ca_polizas';

        public const ID = 'id';
        public const ID_POLIZA_NOMENCLATURA = 'PolizaNomenclatura_id';
        public const EJERCICIO = 'ejercicio';
        public const MES = 'mes';
        public const DIA = 'dia';
        public const FECHA_CREACION = 'fecha_creacion';
     
        public const CREATED_AT = 'created_at';
        public const UPDATED_AT = 'updated_at';
        public const DELETED_AT = 'deleted_at';

        protected $attributes = [
            self::ID_POLIZA_NOMENCLATURA => null,
            self::EJERCICIO => null,
            self::MES => null,
            self::DIA => null,
            self::FECHA_CREACION => null
        ];

        protected $dates = [
            self::CREATED_AT,
            self::UPDATED_AT,
            self::DELETED_AT
        ];

        protected $casts = [
            self::ID => 'integer',
            self::FECHA_CREACION => 'datetime',
            self::CREATED_AT => 'datetime',
            self::UPDATED_AT => 'datetime',
            self::DELETED_AT => '?datetime'
        ];
}
