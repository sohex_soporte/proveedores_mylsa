<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class DePolizaXml_entity extends Entity
{

        public const TABLE_NAME = 'de_poliza_xml';

        public const ID = 'id';
        public const ID_POLIZA_XML = 'id_poliza_xml';
        public const CONCEPTO = 'concepto';
        public const SUBTOTAL = 'subtotal';
        public const TOTAL = 'total';
        public const IVA = 'iva';
        public const ACTIVO = 'activo';
        public const ARCHIVO_XML = 'archivo';
        public const ARCHIVO_PDF = 'archivo_pdf';
        public const ASIENTO_ID_TOTAL = 'asiento_id_total';
        public const ASIENTO_ID_SUBTOTAL = 'asiento_id_subtotal';
        public const ASIENTO_ID_IVA = 'asiento_id_iva';
        public const ID_TRANSACCION = 'id_transaccion_cheque';
        public const DESCRIPCION = 'descripcion';
        


        public const CREATED_AT = 'created_at';
        public const UPDATED_AT = 'updated_at';
        public const DELETED_AT = 'deleted_at';

        protected $attributes = [
            self::ID_POLIZA_XML => null,
            self::CONCEPTO => null,
            self::SUBTOTAL => 0,
            self::TOTAL => 0,
            self::IVA => 0,
            self::ACTIVO => 0,
            self::ARCHIVO_XML => null,
            self::ARCHIVO_PDF => null,
            self::ASIENTO_ID_TOTAL => null,
            self::ASIENTO_ID_SUBTOTAL => null,
            self::ASIENTO_ID_IVA => null,
            self::ID_TRANSACCION => null,
            self::DESCRIPCION => null
        ];

        protected $dates = [
            self::CREATED_AT,
            self::UPDATED_AT,
            self::DELETED_AT
        ];

        protected $casts = [
            self::ID => 'integer',
            self::ACTIVO => 'integer',
            self::SUBTOTAL => 'float',
            self::IVA => 'float',
            self::TOTAL => 'float',
            self::CREATED_AT => 'datetime',
            self::UPDATED_AT => 'datetime',
            self::DELETED_AT => '?datetime'
        ];
}
