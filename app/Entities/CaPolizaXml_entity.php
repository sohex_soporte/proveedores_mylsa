<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class CaPolizaXml_entity extends Entity
{

        public const TABLE_NAME = 'ca_poliza_xml';

        public const ID = 'id';
        public const ID_TRANSACCION = 'transaccion_id';
        public const ACTIVO = 'activo';
        public const ID_POLIZA = 'id_poliza';
        public const ID_POLIZA_NOMENCLATURA = 'id_poliza_nomenclatura';
        public const ID_PERSONA = 'id_persona';
        public const DESCRIPCION = 'descripcion';
        public const ID_ESTATUS_POLIZA_XML = 'id_estatus_poliza_xml';

        public const CREATED_AT = 'created_at';
        public const UPDATED_AT = 'updated_at';
        public const DELETED_AT = 'deleted_at';

        protected $attributes = [
            self::ID_TRANSACCION => null,
            self::ACTIVO => 0,
            self::ID_POLIZA => null,
            self::ID_POLIZA_NOMENCLATURA => null,
            self::ID_PERSONA => null,
            self::DESCRIPCION => null,
            self::ID_ESTATUS_POLIZA_XML => CaEstatusPolizaXml_entity::ID_ESTATUS_PRELLENADO
        ];

        protected $dates = [
            self::CREATED_AT,
            self::UPDATED_AT,
            self::DELETED_AT
        ];

        protected $casts = [
            self::ID => 'integer',
            self::ACTIVO => 'integer',
            self::CREATED_AT => 'datetime',
            self::UPDATED_AT => 'datetime',
            self::DELETED_AT => '?datetime'
        ];
}
