<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class CaAsientos_entity extends Entity
{

        public const TABLE_NAME = 'asientos';

        public const ID = 'id';
        public const ID_POLIZA = 'polizas_id';
        public const ID_ESTATUS = 'estatus_id';
        public const ID_TRANSACCION = 'transaccion_id';
        public const ID_CLIENTE = 'cliente_id';
        public const ID_TIPO_PAGO = 'tipo_pago_id';
        public const ID_CUENTA = 'cuenta';
        public const CARGO = 'cargo';
        public const ABONO = 'abono';
        public const ACUMULADO = 'acumulado';
        public const CONCEPTO = 'concepto';
        public const FECHA_CREACION = 'fecha_creacion';
        public const ID_ORIGEN_TRANSACCION = 'origen_transaccion_id';
        public const VENDEDOR = 'vendedor';
        
        public const CREATED_AT = 'created_at';
        public const UPDATED_AT = 'updated_at';
        public const DELETED_AT = 'deleted_at';

        public const ESTATUS_TEMPORAL = 'TEMPORAL';

        protected $attributes = [
            self::ID_TRANSACCION => null,
            self::ID_ESTATUS => self::ESTATUS_TEMPORAL,
            self::ID_POLIZA => null,
        ];

        protected $dates = [
            self::CREATED_AT,
            self::UPDATED_AT,
            self::DELETED_AT
        ];

        protected $casts = [
            self::ID => 'integer',
            self::CREATED_AT => 'datetime',
            self::UPDATED_AT => 'datetime',
            self::DELETED_AT => '?datetime'
        ];
}
