<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class VwPollizasXml_entity extends Entity
{

    public const TABLE_NAME = 'vw_pollizas_xml';

    public const ID = 'id_poliza';
    public const ID_TRANSACCION = 'id_transaccion';
    public const ID_CLIENTE = 'cliente_id';
    public const ID_PROVEEDOR = 'proveedor_id';
    public const FOLIO = 'folio';
    public const ORIGEN = 'origen';
    public const ID_PERSONA = 'persona_id';
    public const FECHA = 'fecha';
    public const ID_POLIZA_FIJA = 'poliza_fija_id';
    public const ID_SUCURSAL = 'sucursal_id';
    public const ID_ESTATUS = 'estatus_id';
    public const ID_ANTICIPO = 'anticipo_id';
    public const POLIZA = 'poliza';
    public const POLIZA_NOMENCLATURA = 'polizas_nomenclatura';
    public const ABONOS = 'abonos';
    public const CARGOS = 'cargos';
    public const FECHA_DE_POLIZA_XML = 'fecha_de_poliza_xml';

    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const DELETED_AT = 'deleted_at';

    protected $attributes = [
        self::ID_TRANSACCION => null,
        self::ID_CLIENTE => null,
        self::ID_PROVEEDOR => null
    ];

    protected $dates = [
        self::CREATED_AT,
        self::UPDATED_AT,
        self::DELETED_AT
    ];

    protected $casts = [
        self::ID => 'integer',
        self::FECHA => 'datetime',
        self::ABONOS => 'float',
        self::CARGOS => 'float',
        self::FECHA_DE_POLIZA_XML => '?datetime',
        self::CREATED_AT => 'datetime',
        self::UPDATED_AT => 'datetime',
        self::DELETED_AT => '?datetime'
    ];
}
