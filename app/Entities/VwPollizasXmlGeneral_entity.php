<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class VwPollizasXmlGeneral_entity extends Entity
{

    public const TABLE_NAME = 'vw_pollizas_xml_general';

    public const ID = 'id_poliza';
    public const ID_PERSONA = 'persona_id';
    public const POLIZA_NOMENCLATURA = 'polizas_nomenclatura';
    public const POLIZA_XML_DESCRIPCION = 'poliza_xml_descripcion';
    public const SUBTOTAL = 'subtotal';
    public const TOTAL = 'total';
    public const IVA = 'iva';
    public const ID_ESTATUS_POLIZA_XML = 'id_estatus_poliza_xml';
    public const ESTATUS_POLIZA_XML = 'estatus_poliza_xml';

    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const DELETED_AT = 'deleted_at';

    protected $attributes = [
        self::ID_PERSONA => null,
        self::POLIZA_NOMENCLATURA => null,
        self::POLIZA_XML_DESCRIPCION => null
    ];

    protected $dates = [
        self::CREATED_AT,
        self::UPDATED_AT,
        self::DELETED_AT
    ];

    protected $casts = [
        self::ID => 'integer',
        self::ID_PERSONA => 'integer',
        self::TOTAL => 'float',
        self::SUBTOTAL => 'float',
        self::IVA => 'float',
        self::ID_ESTATUS_POLIZA_XML => 'integer',
        self::CREATED_AT => 'datetime',
        self::UPDATED_AT => 'datetime',
        self::DELETED_AT => '?datetime'
    ];
}
