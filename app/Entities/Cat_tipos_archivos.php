<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class Cat_tipos_archivos extends Entity
{

        public const ID = 'id';
        public const NOMBRE = 'nombre';

        public const ID_DOCUMENTO_PDF_ORIGINAL = 1;
        public const ID_DOCUMENTO_PDF_FIRMADO = 2;
        public const ID_DOCUMENTO_XML = 3;

        protected $attributes = [
                self::ID => null,
                self::NOMBRE => null
        ];

        protected $casts = [
                self::ID => 'integer',
                self::NOMBRE => 'string'
        ];

}
