<?php

namespace App\Entities;

use Michalsn\Uuid\UuidEntity;

class Tbl_firmantes extends UuidEntity
{

        public const TABLE_NAME = 'tbl_firmantes';

        public const ID = 'id';
        public const NOMBRE = 'nombre';
        public const CORREO_ELECTRONICO = 'correo_electronico';
        
        public const CREATED_AT = 'created_at';
        public const UPDATED_AT = 'updated_at';
        public const DELETED_AT = 'deleted_at';

        protected $attributes = [
                self::NOMBRE => null,
                self::CORREO_ELECTRONICO => null
        ];

        protected $dates = [
                self::CREATED_AT,
                self::UPDATED_AT,
                self::DELETED_AT
        ];

        protected $uuids = [
                self::ID
        ];

        protected $casts = [
                self::ID => 'uuid',
                self::NOMBRE => 'string',
                self::CORREO_ELECTRONICO => 'string',

                self::CREATED_AT => 'datetime',
                self::UPDATED_AT => 'datetime',
                self::DELETED_AT => '?datetime'
        ];
}
