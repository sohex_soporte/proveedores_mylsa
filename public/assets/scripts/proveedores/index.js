class CLASS_APP {

    formatMoney = null;

    constructor() {
        this.formatMoney = new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2,
        });

        setTimeout(() => {
            app.setCookie('general_descripcion', '', 1 );    
        }, 500);
        
    }

    setCookie(cname, cvalue, exdays) {
        const d = new Date();
        d.setTime(d.getTime() + (exdays * 60 * 60 * 1000));
        let expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";SameSite=Lax;path=/";
    }
      
    getCookie(cname) {
        let name = cname + "=";
        let ca = document.cookie.split(';');
        for(let i = 0; i < ca.length; i++) {
          let c = ca[i];
          while (c.charAt(0) == ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
        }
        return "";
    }

    eliminar($obj) {

        Swal.fire({
            title: 'Estás seguro que deseas eliminar el registro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sí',
            cancelButtonText: `Cancelar`,
        }).then((result) => {

            if (result.isConfirmed) {
                $.ajax({
                    url: PATH + 'apis/eliminar_transaccion_xml',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        poliza: $($obj).data('poliza')
                    },
                    success: function (result, status, xhr) {
                        if (result.status == 'ok') {
                            window.location.reload();
                        }
                    }
                });

            }
        });

    }
}

const app = new CLASS_APP();