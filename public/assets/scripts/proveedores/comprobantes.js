class CLASS_APP {

    formatMoney = null;

    constructor() {
        this.formatMoney = new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2,
        });
    }

    guardar() {

        var data_send = new FormData();
        data_send.append("archivo_pdf", document.getElementById('archivo_pdf').files[0]);
        data_send.append("archivo_xml", document.getElementById('archivo_xml').files[0]);
        data_send.append("descripcion", document.getElementById('descripcion').value);
        data_send.append("total", document.getElementById('total').value);
        data_send.append("iva", document.getElementById('iva').value);
        data_send.append("subtotal", document.getElementById('subtotal').value);
        data_send.append("id_poliza_xml", identity);

        $('small.text-danger').html('');

        $.ajax({
            type: 'POST',
            url: PATH+'apis/comprobante_guardar',
            data: data_send,
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            // beforeSend: function(){
            //     $('small.text-danger').html('');
            // },
            success: function(response){
                if(response.status == 'ok'){
                    
                    Swal.fire({
                        title: 'El comprobante fue guardado correctamente',
                      }).then((result) => {
                        window.location.href = PATH+"proveedores/transacciones/"+btoa(identity);
                      })

                }else{
                    try {
                        $.each(response.message, function( index, value ) {
                            $('small#msg_'+index).html(value);
                        });
                    } catch (error) {
                        
                    }
                }
                // $('#fupForm').css("opacity","");
                // $(".submitBtn").removeAttr("disabled");
            }
        });

        // $('form#form_content').submit();
    }

    leer_xml() {
        var file = document.getElementById("archivo_xml").files[0];
        if (file) {
            var reader = new FileReader();
            reader.readAsText(file, "UTF-8");
            reader.onload = function (evt) {
                var xml = evt.target.result;

                $.ajax({
                    url: PATH+'apis/ver_contenido_xml',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        archivo: btoa(xml)
                    },
                    success: function (result,status,xhr) {
                        if(result.status == 'ok'){
                            var total = app.formatMoney.format(result.data.total);
                            var subtotal = app.formatMoney.format(result.data.subtotal);
                            var iva = app.formatMoney.format(result.data.iva);

                            $('input#total').val(result.data.total);
                            $('input#subtotal').val(result.data.subtotal);
                            $('input#iva').val(result.data.iva);
                            $('input#total_2').val(total);
                            $('input#subtotal_2').val(subtotal);
                            $('input#iva_2').val(iva);



                        }
                        
                    }
                });

            
            }
            reader.onerror = function (evt) {
                
            }
        }   
    }

    eliminar($obj) {

        Swal.fire({
            title: 'Estás seguro que deseas eliminar el registro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sí',
            cancelButtonText: `Cancelar`,
        }).then((result) => {

            if (result.isConfirmed) {
                $.ajax({
                    url: PATH + 'apis/eliminar_contenido_xml',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        poliza: $($obj).data('poliza')
                    },
                    success: function (result, status, xhr) {
                        if (result.status == 'ok') {
                            window.location.reload();
                        }
                    }
                });

            }
        });

    }
}

const app = new CLASS_APP();