class CLASS_APP {

    formatMoney = null;

    constructor() {
        this.formatMoney = new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2,
        });

        try {
            setTimeout(() => {
                var cotntenid = app.getCookie("general_descripcion");
                $('textarea[name=descripcion]').html(cotntenid)
            }, 100);
        } catch (error) {}

        $('textarea[name=descripcion]').blur(function(){
            // setTimeout(() => {
                var cotntenid = $('textarea[name=descripcion]').val();
                app.setCookie('general_descripcion', cotntenid, 1 );
            // }, 100);
        });
    }

    setCookie(cname, cvalue, exdays) {
        const d = new Date();
        d.setTime(d.getTime() + (exdays * 60 * 60 * 1000));
        let expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";SameSite=Lax;path=/";
    }
      
    getCookie(cname) {
        let name = cname + "=";
        let ca = document.cookie.split(';');
        for(let i = 0; i < ca.length; i++) {
          let c = ca[i];
          while (c.charAt(0) == ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
        }
        return "";
    }

    guardar() {

        $.ajax({
            type: 'POST',
            url: PATH + 'apis/transaccion_guardar',
            data: {
                identity: identity,
                descripcion: $('textarea[name=descripcion]').val()
            },
            dataType: 'json',
            beforeSend: function () {
                $('small.text-danger').html('');
            },
            success: function (response) {
                if (response.status == 'ok') {

                    Swal.fire({
                        title: 'El registro fue guardado correctamente',
                    }).then((result) => {
                        window.location.href = PATH + "proveedores";
                    })

                } else {
                    try {
                        $.each(response.message, function (index, value) {
                            $('small#msg_' + index).html(value);
                        });
                    } catch (error) {

                    }
                }
            }
        });
    }


    guardar_enviar() {

        $.ajax({
            type: 'POST',
            url: PATH + 'apis/transaccion_guardar_enviar',
            data: {
                identity: identity,
                descripcion: $('textarea[name=descripcion]').val()
            },
            dataType: 'json',
            beforeSend: function () {
                $('small.text-danger').html('');
            },
            success: function (response) {
                if (response.status == 'ok') {

                    Swal.fire({
                        title: 'El registro fue enviado correctamente',
                    }).then((result) => {
                        window.location.href = PATH + "proveedores";
                    })

                } else {
                    try {
                        $.each(response.message, function (index, value) {
                            $('small#msg_' + index).html(value);
                        });
                    } catch (error) {

                    }
                }
            }
        });
    }

    eliminar($obj) {

        Swal.fire({
            title: 'Estás seguro que deseas eliminar el registro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sí',
            cancelButtonText: `Cancelar`,
        }).then((result) => {

            if (result.isConfirmed) {
                $.ajax({
                    url: PATH + 'apis/eliminar_transaccion_xml',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        poliza: $($obj).data('poliza')
                    },
                    success: function (result, status, xhr) {
                        if (result.status == 'ok') {
                            window.location.reload();
                        }
                    }
                });

            }
        });

    }
}

const app = new CLASS_APP();